<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model {
	protected $table = "banner";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'title_1', 'title_en_1', 'link_1', 'image_1', 'title_2', 'title_en_2', 'link_2', 'image_2', 'title_3', 'title_en_3', 'link_3', 'image_3', 'title_4', 'title_en_4', 'link_4', 'image_4', 'title_5', 'title_en_5', 'link_5', 'image_5', 'title_6', 'title_en_6', 'link_6', 'image_6', 'status',
	];

	public $timestamps = true;

	public function getTitle1() {
		return local_value($this->title_1, $this->title_en_1);
	}

	public function getTitle2() {
		return local_value($this->title_2, $this->title_en_2);
	}

	public function getTitle3() {
		return local_value($this->title_3, $this->title_en_3);
	}

	public function getTitle4() {
		return local_value($this->title_4, $this->title_en_4);
	}

	public function getTitle5() {
		return local_value($this->title_4, $this->title_en_4);
	}

	public function getTitle6() {
		return local_value($this->title_4, $this->title_en_4);
	}

	public function getLink1() {
		if ($this->link_1 == "") {
			return "javascript:void(0)";
		}
		return $this->link_1;
	}

	public function getLink2() {
		if ($this->link_2 == "") {
			return "javascript:void(0)";
		}
		return $this->link_2;
	}

	public function getLink3() {
		if ($this->link_3 == "") {
			return "javascript:void(0)";
		}
		return $this->link_3;
	}

	public function getLink4() {
		if ($this->link_4 == "") {
			return "javascript:void(0)";
		}
		return $this->link_4;
	}

	public function getLink5() {
		if ($this->link_5 == "") {
			return "javascript:void(0)";
		}
		return $this->link_5;
	}

	public function getLink6() {
		if ($this->link_6 == "") {
			return "javascript:void(0)";
		}
		return $this->link_6;
	}

	/**
	 * Get banner by cate id
	 *
	 * @var id = banner id, id default is 1
	 * @return item banner
	 */
	public static function getBanner($id) {
		$banner = Banner::all()
			->where('id', $id)
			->where('status', true)
			->first();
		return $banner;
	}
}
