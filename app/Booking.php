<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model {
	protected $table = "booking";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'full_name', 'phone_number', 'email', 'zalo', 'facebook', 'status', 'is_deleted', 'created_at', 'updated_at',
	];

	public $timestamps = true;
}
