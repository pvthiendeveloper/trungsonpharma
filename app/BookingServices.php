<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingServices extends Model {
	protected $table = "booking_services";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'booking_id', 'services_id', 'status', 'is_deleted',
	];

	public $timestamps = true;
}
