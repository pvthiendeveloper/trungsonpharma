<?php

namespace App;

use Debugbar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model {
	protected $table = "categories";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'parent_id', 'name', 'name_en', 'alias', 'alias_en', 'order', 'status', 'is_deleted', 'created_at', 'updated_at',
	];

	public $timestamps = true;

	public function getTableName() {
		return $this->table;
	}

	public static function getModelNotFound() {
		return self::$MODEL_NOT_FOUND;
	}

	public function getCategoryChilds() {
		$childs = Category::all()
			->where('parent_id', $this->id)
			->where('status', true)
			->where('is_deleted', false)
			->sortBy('order'); //sortByDesc
		return $childs;
	}

	public function getCategoryChildsToList() {
		$childs = DB::table('categories')
			->where('parent_id', $this->id)
			->where('status', true)
			->where('is_deleted', false)
			->get();
		return $childs;
	}

	public function getName() {
		return local_value($this->name, $this->name_en);
	}

	public function getAlias() {
		return local_value($this->alias, $this->alias_en);
	}

	public function getNewsChilds() {
		$childs = News::where('cate_id', $this->id)
			->where('status', true)
			->where('is_deleted', false)
			->orderBy('order', 'asc')
			->paginate(9);
		return $childs;
	}

	public function getAllNewsChilds() {
		Debugbar::info("Category id: " . $this->id);
		$categories = DB::table('categories')
			->where('parent_id', $this->id)
			->where('status', true)
			->where('is_deleted', false)
			->get();

		$cateIds = array();
		$cateIds[] = $this->id;
		foreach ($categories as $item) {
			$cateIds[] = $item->id;
		}

		Debugbar::info($cateIds);

		$childs = News::where('status', true)
			->where('is_deleted', false)
			->whereIn('cate_id', $cateIds)
			->orderBy('order', 'asc')
			->paginate(9);
		return $childs;
	}

	public function getRelatedPosts($newId) {
		$childs = News::where('cate_id', $this->id)
			->where('id', '!=', $newId)
			->where('status', true)
			->where('is_deleted', false)
			->orderBy('order', 'asc');
		return $childs;
	}

	/**
	 * Get category by cate id
	 *
	 * @var cateId
	 * @return item category
	 */
	public static function getCategory($cateId) {
		$category = Category::all()
			->where('id', $cateId)
			->where('status', true)
			->where('is_deleted', false)
			->first();
		return $category;
	}

	/**
	 * Get category by alias
	 *
	 * @var alias
	 * @return item category
	 */
	public static function getCategoryByAlias($alias) {
		$category = Category::where(function ($query) use ($alias) {
			return $query->where('alias', $alias)
				->orWhere('alias_en', $alias);
		})
			->where('status', true)
			->where('is_deleted', false)
			->first();
		return $category;
	}

	/**
	 * Get category orthers by alias
	 *
	 * @var alias
	 * @return item category orthers
	 */
	public static function getCategoryOthers($alias) {
		$category = Category::getCategoryByAlias($alias);
		if ($category == null) {
			return null;
		}
		$cateList = $category->getCategoryChilds()->take(3);
		return $cateList;
	}
}
