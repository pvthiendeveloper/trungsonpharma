<?php

if (!function_exists('local_value')) {
	// *
	// * Generate the URL to a named route.
	// *
	// * @param string $name
	// * @param array $parameters
	// * @param bool $absolute
	// * @param null|string $locale
	// *
	// * @return string

	function local_value($valueVi, $valueEn) {
		$lang = trans('messages.lang');
		if ($lang == 'vi') {
			return $valueVi;
		}

		return $valueEn;
	}
}
