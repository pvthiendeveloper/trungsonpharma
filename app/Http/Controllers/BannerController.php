<?php

namespace App\Http\Controllers;
use App\Banner;
use App\Http\Requests\EditBannerRequest;

class BannerController extends Controller {
	public static $BANNER_ID = 1;

	public function getEditBanner() {
		$banner = Banner::getBanner(self::$BANNER_ID);
		return view('admin.banner.banner', ['banner' => $banner]);
	}

	public function postEditBanner(EditBannerRequest $request) {
		if ($request->validated()) {
			$banner = Banner::find(self::$BANNER_ID);
			$banner->title_1 = $request->title_1;
			$banner->title_en_1 = $request->title_en_1;
			$banner->link_1 = $request->link_1;
			$banner->image_1 = $request->image_1;

			$banner->title_2 = $request->title_2;
			$banner->title_en_2 = $request->title_en_2;
			$banner->link_2 = $request->link_2;
			$banner->image_2 = $request->image_2;

			$banner->title_3 = $request->title_3;
			$banner->title_en_3 = $request->title_en_3;
			$banner->link_3 = $request->link_3;
			$banner->image_3 = $request->image_3;

			$banner->title_4 = $request->title_4;
			$banner->title_en_4 = $request->title_en_4;
			$banner->link_4 = $request->link_4;
			$banner->image_4 = $request->image_4;

			$banner->title_5 = $request->title_5;
			$banner->title_en_5 = $request->title_en_5;
			$banner->link_5 = $request->link_5;
			$banner->image_5 = $request->image_5;

			$banner->title_6 = $request->title_6;
			$banner->title_en_6 = $request->title_en_6;
			$banner->link_6 = $request->link_6;
			$banner->image_6 = $request->image_6;

			$banner->save();
			return view('admin.banner.banner', ['banner' => $banner]);
		}
		return view('admin.banner.banner');
	}
}
