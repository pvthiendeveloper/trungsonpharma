<?php

namespace App\Http\Controllers;
use App\Booking;
use App\BookingServices;
use App\Category;
use App\Http\Requests\BookingRequest;
use DB;

class BookingController extends Controller {

	public static $CATEGORY_SERVICES_ID = 3;

	public function getListBooking() {
		$booking = DB::table('booking')->where('is_deleted', false)->orderBy('id', 'desc')->get();
		return view('admin.booking.list-booking', ['booking' => $booking]);
	}

	public function getDeleteBooking($id) {
		$booking = Booking::find($id);
		$booking->is_deleted = true;
		$booking->save();
		return response()->json(['error' => false, 'message' => trans('messages.msg_delete_booking_successfully')]);
	}

	public function getEditBooking($id) {
		$booking = Booking::find($id);

		$category = Category::getCategory(self::$CATEGORY_SERVICES_ID);
		$services = $category->getNewsChilds();

		$bookingServices = BookingServices::where('booking_id', $booking->id)
			->where('status', true)
			->where('is_deleted', false)
			->get();

		return view('admin.booking.edit-booking', ['booking' => $booking, 'services' => $services, 'bookingServices' => $bookingServices]);
	}

	public function postEditBooking(BookingRequest $request, $id) {
		if ($request->validated()) {

			$booking = Booking::find($id);
			$booking->full_name = $request->fullName;
			$booking->phone_number = $request->phoneNumber;
			$booking->email = $request->email;
			$booking->zalo = $request->zalo;
			$booking->facebook = $request->facebook;
			$booking->message = $request->message;
			$booking->status = $request->status;
			$booking->is_deleted = false;
			$booking->save();

			$bookingServices = BookingServices::where('booking_id', $booking->id)
				->delete();

			$services = $request->service;
			if ($services != null) {
				foreach ($services as $key => $value) {
					$bookingServices = new BookingServices();
					$bookingServices->booking_id = $booking->id;
					$bookingServices->services_id = $value;
					$bookingServices->status = true;
					$bookingServices->is_deleted = false;
					$bookingServices->save();
				}
			}

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_edit_booking_successfully'),
			];

			return redirect()->route('listBooking')->with('toastData', $toastData);
		}
		return view('admin.booking.edit-booking');
	}
}
