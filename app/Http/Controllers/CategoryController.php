<?php

namespace App\Http\Controllers;
use App\Category;
use App\Http\Requests\AddCategoryRequest;
use DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends ParentController {

	public function getListCate() {
		$categories = DB::table('categories')->where('is_deleted', false)->get();
		return view('admin.cate.list-cate', ['categories' => $categories]);
	}

	public function getCreateCate() {
		$categories = DB::table('categories')->where('status', 1)->where('is_deleted', 0)->get();
		return view('admin.cate.add-cate', ['categories' => $categories]);
	}

	public function postCreateCate(AddCategoryRequest $request) {
		if ($request->validated()) {
			$validator = Validator::make([], []);
			$validator->after(function ($validator) use ($request) {

				//Check username is exist
				$cateNameIsExist = DB::table('categories')->where('name', $request->categoryName)->where('is_deleted', false)->count() > 0;
				if ($cateNameIsExist) {
					$validator->errors()->add('categoryName', trans('messages.error_cate_name_is_exist'));
				}

				//Check username is exist
				$cateNameEnIsExist = DB::table('categories')->where('name_en', $request->categoryNameEn)->where('is_deleted', false)->count() > 0;
				if ($cateNameEnIsExist) {
					$validator->errors()->add('categoryNameEn', trans('messages.error_cate_name_en_is_exist'));
				}
			});

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$category = new Category();
			$category->name = $request->categoryName;
			$category->name_en = $request->categoryNameEn;
			$category->parent_id = $request->parentId;
			$category->alias = str_slug($request->categoryName);
			$category->alias_en = str_slug($request->categoryNameEn);
			$category->order = $request->order;
			$category->status = $request->status;
			$category->is_deleted = false;
			$category->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_add_category_successfully'),
			];

			return redirect()->route('listCategory')->with('toastData', $toastData);
		}
		return view('admin.cate.add-cate');
	}

	public function getEditCate($id) {
		$category = DB::table('categories')->where('id', $id)->where('is_deleted', 0)->first();
		$categories = DB::table('categories')->where('is_deleted', 0)->get();
		return view('admin.cate.edit-cate', ['category' => $category, 'categories' => $categories]);
	}

	public function postEditCate(AddCategoryRequest $request, $id) {
		if ($request->validated()) {
			$validator = Validator::make([], []);
			$validator->after(function ($validator) use ($request, $id) {

				//Check username is exist
				$cateNameIsExist = DB::table('categories')->where('name', $request->categoryName)->where('id', '!=', $id)->where('is_deleted', false)->count() > 0;
				if ($cateNameIsExist) {
					$validator->errors()->add('categoryName', trans('messages.error_cate_name_is_exist'));
				}

				//Check username is exist
				$cateNameEnIsExist = DB::table('categories')->where('name_en', $request->categoryNameEn)->where('id', '!=', $id)->where('is_deleted', false)->count() > 0;
				if ($cateNameEnIsExist) {
					$validator->errors()->add('categoryNameEn', trans('messages.error_cate_name_en_is_exist'));
				}
			});

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$category = Category::find($id);
			$category->name = $request->categoryName;
			$category->name_en = $request->categoryNameEn;
			$category->parent_id = $request->parentId;
			$category->alias = str_slug($request->categoryName);
			$category->alias_en = str_slug($request->categoryNameEn);
			$category->order = $request->order;
			$category->status = $request->status;
			$category->is_deleted = false;
			$category->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_edit_cate_successfully'),
			];

			return redirect()->route('listCategory')->with('toastData', $toastData);
		}
		return view('admin.cate.edt-cate');
	}

	public function getDeleteCate($id) {
		$category = Category::find($id);
		$category->is_deleted = true;
		$category->save();
		return response()->json(['error' => false, 'message' => trans('messages.msg_delete_cate_successfully')]);
	}
}
