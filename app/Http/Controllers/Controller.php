<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use View;

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	function __construct() {
		$categories = Category::all()
			->where('status', true)
			->where('id', '!=', 7)
			->where('is_deleted', false)->sortBy('order'); //sortBy or sortByDesc
		view()->share('categories', $categories);
	}
}