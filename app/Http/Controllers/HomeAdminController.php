<?php

namespace App\Http\Controllers;
use App\Booking;
use App\Category;
use App\News;
use App\User;
use Auth;
use Request;
use Route;

class HomeAdminController extends Controller {
	public function getHomeAdminPage(Request $request) {
		$toalAccount = User::all()->count();
		$totalCate = Category::all()->where('is_deleted', false)->count();
		$totalNews = News::all()->where('is_deleted', false)->count();
		$totalBooking = Booking::all()->where('is_deleted', false)->count();
		return view('admin.home.home', ['toalAccount' => $toalAccount, 'totalCate' => $totalCate, 'totalNews' => $totalNews, 'totalBooking' => $totalBooking]);
	}

	public function getLogout() {
		Auth::logout();
		return redirect()->route('login');
	}
}
