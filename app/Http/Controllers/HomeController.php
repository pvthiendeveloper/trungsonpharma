<?php

namespace App\Http\Controllers;
use App\Banner;
use App\Booking;
use App\BookingServices;
use App\Category;
use App\Images;
use App\Http\Requests\BookingRequest;
use App\Settings;

class HomeController extends Controller {

	public static $BANNER_ID = 1;
	public static $CATEGORY_SERVICES_ID = 3;
	public static $CATEGORY_NEWS_HOT_ID = 7;
	public static $SETTING_ID = 1;
	public static $IMAGES_TYPE = "ACTIVITY";

	function __construct() {
		parent::__construct();
		$banner = Banner::getBanner(self::$BANNER_ID);

		$category = Category::getCategory(self::$CATEGORY_SERVICES_ID);
		$services = $category->getAllNewsChilds();

		$hot = Category::getCategory(self::$CATEGORY_NEWS_HOT_ID);
		$newsHot = $hot->getAllNewsChilds();

		$imagesActivity = Images::getImagesByType(self::$IMAGES_TYPE);

		$bookServices = $category->getCategoryChildsToList();

		view()->share(['banner' => $banner, 'services' => $services, 'bookServices' => $bookServices, 'newsHot' => $newsHot, 'imagesActivity' => $imagesActivity]);
	}

	public function getHomePage() {
		$settings = Settings::getSettings(self::$SETTING_ID);
		return view('client.home.home', ['settings' => $settings]);
	}

	public function getBookingPage() {
		$url_vi = "/dang-ky-tu-van";
		$url_en = "/booking";
		return view('client.booking.booking', ['url_vi' => $url_vi, 'url_en' => $url_en]);
	}

	public function postCreateBooking(BookingRequest $request) {
		if ($request->validated()) {
			$booking = new Booking();
			$booking->full_name = $request->fullName;
			$booking->phone_number = $request->phoneNumber;
			$booking->email = $request->email;
			$booking->zalo = $request->zalo;
			$booking->facebook = $request->facebook;
			$booking->message = $request->message;
			$booking->status = "Chờ xử lý";
			$booking->is_deleted = false;
			$booking->save();

			$services = $request->service;
			if ($services != null) {
				foreach ($services as $key => $value) {
					$bookingServices = new BookingServices();
					$bookingServices->booking_id = $booking->id;
					$bookingServices->services_id = $value;
					$bookingServices->status = true;
					$bookingServices->is_deleted = false;
					$bookingServices->save();
				}
			}

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_booking_successfully'),
			];

			return redirect()->route('home')->with('toastData', $toastData);
		}
		$url_vi = "/dang-ky-tu-van";
		$url_en = "/booking";
		return view('client.booking.booking', ['url_vi' => $url_vi, 'url_en' => $url_en]);
	}

	public function getDoctorHuyPage() {
		$url_vi = "/bac-si/ths-bs-ly-quang-huy";
		$url_en = "/doctor/dr-huy";
		return view('client.doctor.dr-huy', ['url_vi' => $url_vi, 'url_en' => $url_en]);
	}

	public function getDoctorGiauPage() {
		$url_vi = "/bac-si/ths-bs-nguyen-huu-giau";
		$url_en = "/doctor/dr-giau";
		return view('client.doctor.dr-giau', ['url_vi' => $url_vi, 'url_en' => $url_en]);
	}

	public function getDoctorViPage() {
		$url_vi = "/bac-si/bs-tran-vo-thuy-vy";
		$url_en = "/doctor/dr-vy";
		return view('client.doctor.dr-vi', ['url_vi' => $url_vi, 'url_en' => $url_en]);
	}
}
