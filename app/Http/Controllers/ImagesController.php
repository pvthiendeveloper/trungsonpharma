<?php

namespace App\Http\Controllers;

use App\Images;
use DB;
use App\Http\Requests\AddImageRequest;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    public static $ACTIVITY_IMAGE_TYPE = 'ACTIVITY';

    public function getListImages() {
		$images = Images::getImages();
		return view('admin.images.list-images', ['images' => $images]);
	}

    public function getCreateImages() {
		return view('admin.images.add-images');
	}

    public function postCreateImages(AddImageRequest $request) {
    	if ($request->validated()) {

    		$image = new Images();
			$image->image_url = $request->image;
			$image->image_type = "ACTIVITY";
			$image->status = $request->status;
			$image->is_deleted = false;
			$image->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_add_image_successfully'),
			];

			return redirect()->route('listImages')->with('toastData', $toastData);
    	}
		return view('admin.images.add-images');
	}

	public function getEditImages($id) {
		$image = DB::table('images')->where('id', $id)->first();
		return view('admin.images.edit-images', ['image' => $image]);
	}

	public function postEditImages(AddImageRequest $request, $id) {
		if ($request->validated()) {

    		$image = Images::find($id);
			$image->image_url = $request->image;
			$image->image_type = "ACTIVITY";
			$image->status = $request->status;
			$image->is_deleted = false;
			$image->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_edit_image_successfully'),
			];

			return redirect()->route('listImages')->with('toastData', $toastData);
    	}
		return view('admin.images.edit-images', ['image' => $image]);
	}

	public function getDeleteImages($id) {
		$image = Images::find($id);
		$image->is_deleted = true;
		$image->save();
		return response()->json(['error' => false, 'message' => trans('messages.msg_delete_images_successfully')]);
	}
}
