<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class LanguageController extends Controller {

	public function getSetLocalizeVi(Request $request) {
		App::setlocale('vi');
		return redirect()->back();
	}

	public function getSetLocalizeEn(Request $request) {
		App::setlocale('en');
		//dd(redirect()->back()->getRequest()->segment(1));
		return redirect()->back();
	}
}
