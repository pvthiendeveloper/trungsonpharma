<?php

namespace App\Http\Controllers;
use App\Category;
use App\News;

class NewsClientController extends Controller {

	public static $CATEGORY_SERVICES_ID = 3;
	public static $CATEGORY_NEWS_ID = 5;
	public static $CATEGORY_TECHNOLOGY_ID = 4;
	public static $CATEGORY_KNOWLEDGE_OF_BEAUTY_ID = 6;

	function __construct() {
		parent::__construct();
		/*Get services, id = 3 is id of services*/
		$category = Category::getCategory(self::$CATEGORY_SERVICES_ID);
		$services = $category->getAllNewsChilds()->take(5);
		view()->share(['services' => $services]);
	}

	public function getServices() {
		$category = Category::getCategory(self::$CATEGORY_SERVICES_ID);
		// $newsChild = $category->getAllNewsChilds();
		$newsChild = $category->getAllNewsChilds();
		$url_vi = "/dich-vu";
		$url_en = "/services";
		$title = $category->getName();

		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

	public function getServicesDetail($alias) {
		$category = Category::getCategoryByAlias($alias);

		/*Trường hợp menu này không phải là cate mà là 1 dịch vụ*/
		if ($category == null) {
			$news = News::getNewsByAlias($alias);
			if ($news == null) {
				return redirect('/');
			}
			$url_vi = "/dich-vu/" . $news->alias;
			$url_en = "/services/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		$newsChild = $category->getAllNewsChilds();

		if ($newsChild == null) {
			return redirect('/');
		}

		/*Trường hợp chỉ có 1 dịch vụ*/
		if ($newsChild->count() == 1) {
			$news = $newsChild->first();
			$url_vi = "/dich-vu/" . $news->alias;
			$url_en = "/services/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		/*Trường hợp có nhiều dịch vụ*/
		$title = $category->getName();
		$url_vi = "/dich-vu/" . $category->alias;
		$url_en = "/services/" . $category->alias_en;

		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

	public function getNews() {
		$category = Category::getCategory(self::$CATEGORY_NEWS_ID);
		$newsChild = $category->getAllNewsChilds();
		$url_vi = "/tin-tuc";
		$url_en = "/news";
		$title = $category->getName();
		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

	public function getNewsDetail($alias) {
		$category = Category::getCategoryByAlias($alias);

		if ($category == null) {
			$news = News::getNewsByAlias($alias);
			if ($news == null) {
				return redirect('/');
			}
			$url_vi = "/tin-tuc/" . $news->alias;
			$url_en = "/news/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		$newsChild = $category->getAllNewsChilds();

		if ($newsChild == null) {
			return redirect('/');
		}

		if ($newsChild->count() == 1) {
			$news = $newsChild->first();
			$url_vi = "/tin-tuc/" . $news->alias;
			$url_en = "/news/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		$title = $category->getName();
		$url_vi = "/tin-tuc/" . $category->alias;
		$url_en = "/news/" . $category->alias_en;

		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

	public function getTechnology() {
		$category = Category::getCategory(self::$CATEGORY_TECHNOLOGY_ID);
		$newsChild = $category->getAllNewsChilds();
		$url_vi = "/cong-nghe";
		$url_en = "/technology";
		$title = $category->getName();
		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

	public function getTechnologyDetail($alias) {
		$category = Category::getCategoryByAlias($alias);

		if ($category == null) {
			$news = News::getNewsByAlias($alias);
			if ($news == null) {
				return redirect('/');
			}
			$url_vi = "/cong-nghe/" . $news->alias;
			$url_en = "/technology/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		$newsChild = $category->getAllNewsChilds();

		if ($newsChild == null) {
			return redirect('/');
		}

		if ($newsChild->count() == 1) {
			$news = $newsChild->first();
			$url_vi = "/cong-nghe/" . $news->alias;
			$url_en = "/technology/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		$title = $category->getName();
		$url_vi = "/cong-nghe/" . $category->alias;
		$url_en = "/technology/" . $category->alias_en;

		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

	public function getKnowledgeOfBeauty() {
		$category = Category::getCategory(self::$CATEGORY_KNOWLEDGE_OF_BEAUTY_ID);
		$newsChild = $category->getAllNewsChilds();
		$url_vi = "/" . $category->alias;
		$url_en = "/" . $category->alias_en;
		$title = $category->getName();
		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

	public function getKnowledgeOfBeautyDetail($alias) {
		$category = Category::getCategoryByAlias($alias);

		if ($category == null) {
			$news = News::getNewsByAlias($alias);
			if ($news == null) {
				return redirect('/');
			}
			$url_vi = "/kien-thuc-lam-dep/" . $news->alias;
			$url_en = "/knowledge-of-beauty/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		$newsChild = $category->getAllNewsChilds();

		if ($newsChild == null) {
			return redirect('/');
		}

		if ($newsChild->count() == 1) {
			$news = $newsChild->first();
			$url_vi = "/kien-thuc-lam-dep/" . $news->alias;
			$url_en = "/knowledge-of-beauty/" . $news->alias_en;

			/*Trả về các bài viết liên quan*/
			$relatedPosts = Category::getCategory($news->cate_id)->getRelatedPosts($news->id)->take(3)->get();

			return view('client.news.news-detail', ['news' => $news, 'relatedPosts' => $relatedPosts, 'url_vi' => $url_vi, 'url_en' => $url_en]);
		}

		$title = $category->getName();
		$url_vi = "/kien-thuc-lam-dep/" . $news->alias;
		$url_en = "/knowledge-of-beauty/" . $news->alias_en;

		return view('client.news.list-news', ['newsChild' => $newsChild, 'url_vi' => $url_vi, 'url_en' => $url_en, "title" => $title]);
	}

}
