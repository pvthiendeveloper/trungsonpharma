<?php

namespace App\Http\Controllers;
use App\Http\Requests\AddNewsRequest;
use App\News;
use Auth;
use DB;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller {
	public function getListNews() {
		$news = DB::table('news')->where('is_deleted', false)->get();
		return view('admin.news.list-news', ['news' => $news]);
	}

	public function getCreateNews() {
		$categories = DB::table('categories')->where('is_deleted', false)->get();
		return view('admin.news.add-news', ['categories' => $categories]);
	}

	public function postCreateNews(AddNewsRequest $request) {
		if ($request->validated()) {
			$validator = Validator::make([], []);
			$validator->after(function ($validator) use ($request) {

				//Check title is exist
				$newsTitleIsExist = DB::table('news')->where('title', $request->title)->where('is_deleted', false)->count() > 0;
				if ($newsTitleIsExist) {
					$validator->errors()->add('title', trans('messages.msg_title_is_exist'));
				}

				//Check title_en is exist
				$newsTitleEnIsExist = DB::table('news')->where('title_en', $request->titleEn)->where('is_deleted', false)->count() > 0;
				if ($newsTitleEnIsExist) {
					$validator->errors()->add('titleEn', trans('messages.msg_title_en_is_exist'));
				}
			});

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$news = new News();
			$news->title = $request->title;
			$news->title_en = $request->titleEn;
			$news->alias = str_slug($request->title);
			$news->alias_en = str_slug($request->titleEn);
			$news->short_description = $request->shortDescription;
			$news->short_description_en = $request->shortDescriptionEn;
			$news->content = $request->content;
			$news->content_en = $request->contentEn;
			$news->user_id = Auth::user()->id;
			$news->cate_id = $request->cateId;
			$news->order = $request->order;
			$news->image = $request->image;
			$news->image_2 = $request->image_2;
			$news->status = $request->status;
			$news->is_deleted = false;
			$news->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_add_news_successfully'),
			];

			return redirect()->route('listNews')->with('toastData', $toastData);
		}
		return view('admin.news.add-news');
	}

	public function getEditNews($id) {
		$categories = DB::table('categories')->where('is_deleted', false)->get();
		$news = DB::table('news')->where('id', $id)->first();
		return view('admin.news.edit-news', ['categories' => $categories, 'news' => $news]);
	}

	public function postEditNews(AddNewsRequest $request, $id) {
		if ($request->validated()) {
			$validator = Validator::make([], []);
			$validator->after(function ($validator) use ($request, $id) {

				//Check title is exist
				$newsTitleIsExist = DB::table('news')->where('title', $request->title)->where('is_deleted', false)->where('id', '!=', $id)->count() > 0;
				if ($newsTitleIsExist) {
					$validator->errors()->add('title', trans('messages.msg_title_is_exist'));
				}

				//Check title_en is exist
				$newsTitleEnIsExist = DB::table('news')->where('title_en', $request->titleEn)->where('is_deleted', false)->where('id', '!=', $id)->count() > 0;
				if ($newsTitleEnIsExist) {
					$validator->errors()->add('titleEn', trans('messages.msg_title_en_is_exist'));
				}
			});

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$news = News::find($id);
			$news->title = $request->title;
			$news->title_en = $request->titleEn;
			$news->alias = str_slug($request->title);
			$news->alias_en = str_slug($request->titleEn);
			$news->short_description = $request->shortDescription;
			$news->short_description_en = $request->shortDescriptionEn;
			$news->content = $request->content;
			$news->content_en = $request->contentEn;
			$news->user_id = Auth::user()->id;
			$news->cate_id = $request->cateId;
			$news->order = $request->order;
			$news->image = $request->image;
			$news->image_2 = $request->image_2;
			$news->status = $request->status;
			$news->is_deleted = false;
			$news->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_edit_news_successfully'),
			];

			return redirect()->route('listNews')->with('toastData', $toastData);
		}
		return view('admin.news.edit-news');
	}

	public function getDeleteNews($id) {
		$news = News::find($id);
		$news->is_deleted = true;
		$news->save();
		return response()->json(['error' => false, 'message' => trans('messages.msg_delete_news_successfully')]);
	}
}
