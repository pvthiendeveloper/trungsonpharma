<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Routing\Controller as BaseController;

class ParentController extends BaseController {
	protected $locale = "";

	public function __construct() {
		$this->locale = App::getLocale();
	}
}
