<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditSettingsRequest;
use App\Settings;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Request;

class SettingsController extends BaseController {

	public static $SETTING_ID = 1;

	public function getSettings() {
		return view('admin.settings.settings');
	}

	public function postSettings(Request $request) {
		if (Request::hasFile('file')) {
			$file = Request::file('file');
			$filename = $file->getClientOriginalName();
			$path = public_path() . '/uploads/';
			dd($path);
			return $file->move($path, $filename);
		}
		return view('admin.settings.settings');
	}

	public function getWebSettings() {
		$settings = Settings::getSettings(self::$SETTING_ID);
		return view('admin.settings.web-settings', ['settings' => $settings]);
	}

	public function postWebSettings(EditSettingsRequest $request) {
		if ($request->validated()) {
			$settings = Settings::find(self::$SETTING_ID);
			$settings->introduce_video_url = $request->introduceVideoUrl;
			$settings->status = true;
			$settings->is_deleted = false;
			$settings->save();

			return redirect()->route('dashboard');
		}
		return view('admin.settings.web-settings');
	}
}
