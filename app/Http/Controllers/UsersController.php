<?php

namespace App\Http\Controllers;

use App;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\LoginRequest;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends ParentController {

	public function getLoginAdmin() {
		if (Auth::check()) {
			return redirect()->route('dashboard');
		}
		return view('admin.login.login');
	}

	public function postloginadmin(loginrequest $request) {
		if ($request->validated()) {
			$login = [
				'username' => $request->username,
				'password' => $request->password,
			];
			if (Auth::attempt($login)) {
				$user = Auth::user();
				if ($user->status == 1) {
					return redirect()->route('dashboard');
				} else {
					return view('admin.login.login')->with('messageError', trans('messages.account_has_blocked'));
				}
			}

		}
		return view('admin.login.login')->with('messageError', trans('messages.error_username_and_password_incorrect'));
	}

	public function getListUser() {
		$users = User::all();
		return view('admin.user.list-user', ['users' => $users]);
	}

	public function getCreateUser() {
		return view('admin.user.add-user');
	}

	public function postCreateUser(AddUserRequest $request) {
		if ($request->validated()) {

			$validator = Validator::make([], []);

			$validator->after(function ($validator) use ($request) {

				//Check username is exist
				$userNameIsExist = User::where('username', 'like', $request['username'])->count() > 0;
				if ($userNameIsExist) {
					$validator->errors()->add('username', trans('messages.msg_error_username_is_exist'));
				}

				//Check email is exist
				$emailIsExist = User::where('email', 'like', $request['email'])->count() > 0;
				if ($emailIsExist == true) {
					$validator->errors()->add('email', trans('messages.msg_error_email_is_exist'));
				}
			});

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			$user = new User();
			$user->name = $request->fullname;
			$user->username = $request->username;
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			$user->role = $request->role;
			$user->status = $request->status;
			$user->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_add_account_successfully'),
			];

			return redirect()->route('listAccount')->with('toastData', $toastData);
		}
		return view('admin.user.add-user');
	}

	public function getEditUser($id) {
		$users = User::find($id);
		return view('admin.user.edit-user', ['users' => $users]);
	}

	public function postEditUser(EditUserRequest $request, $id) {
		if ($request->validated()) {
			$validator = Validator::make([], []);

			$validator->after(function ($validator) use ($request, $id) {

				//Check username is exist
				$userNameIsExist = DB::table('users')->where('username', 'like', $request['username'])->where('id', '!=', $id)->count() > 0;
				if ($userNameIsExist) {
					$validator->errors()->add('username', trans('messages.msg_error_username_is_exist'));
				}

				//Check email is exist
				$emailIsExist = DB::table('users')->where('email', 'like', $request['email'])->where('id', '!=', $id)->count() > 0;
				if ($emailIsExist == true) {
					$validator->errors()->add('email', trans('messages.msg_error_email_is_exist'));
				}

				//Check password
				$password = $request->password;
				if (strlen($password) > 0) {
					if (strlen($password) < 6) {
						$validator->errors()->add('password', trans('messages.msg_error_password_min'));
					} else if (strlen($password) > 50) {
						$validator->errors()->add('password', trans('messages.msg_error_password_max'));
					}

					$passwordConfirm = $request->password_confirmation;
					if (strlen($passwordConfirm) == "") {
						$validator->errors()->add('password_confirmation', trans('messages.msg_error_confirm_password_empty'));
					} else if (strlen($passwordConfirm) < 6) {
						$validator->errors()->add('password_confirmation', trans('messages.msg_error_confirm_password_min'));
					} else if (strlen($passwordConfirm) > 50) {
						$validator->errors()->add('password_confirmation', trans('messages.msg_error_confirm_password_max'));
					} else if ($passwordConfirm != $password) {
						$validator->errors()->add('password_confirmation', trans('messages.msg_error_confirm_password_no_matching'));
					}
				}
			});

			if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}

			//Check role
			$userLogin = Auth::user();
			$user = User::find($id);
			if ($userLogin->role >= $user->role || $user->role == 1) {
				$toastData = [
					'type' => 'danger',
					'title' => trans('messages.notification'),
					'message' => trans('messages.msg_no_permission'),
				];

				return redirect()->route('listAccount')->with('toastData', $toastData);
			}

			$user->name = $request->fullname;
			$user->username = $request->username;
			$user->email = $request->email;
			if (strlen($request->password) > 6 && strlen($request->password) < 50) {
				$user->password = bcrypt($request->password);
			}
			$user->role = $request->role;
			$user->status = $request->status;
			$user->save();

			$toastData = [
				'type' => 'success',
				'title' => trans('messages.notification'),
				'message' => trans('messages.msg_edit_account_successfully'),
			];

			return redirect()->route('listAccount')->with('toastData', $toastData);
		}
		return view('admin.user.edit-user');
	}

	public function getDeleteUser($id) {
		//Check role
		$userLogin = Auth::user();
		$user = User::find($id);
		if ($userLogin->role >= $user->role || $user->role == 1) {
			return response()->json(['error' => true, 'message' => trans('messages.msg_no_permission')]);
		}

		$user->delete();
		return response()->json(['error' => false, 'message' => trans('messages.msg_delete_account_successfully')]);
	}
}
