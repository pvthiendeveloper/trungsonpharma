<?php

namespace App\Http\Middleware;

use Closure;
use Debugbar;
use Route;
use View;

class ClientMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {

		Debugbar::info("Url = " . $request->url());

		$path = $request->path();
		$currentUrl = "";
		if ($path == '/') {
			$currentUrl = $request->url();
		} else {
			$currentUrl = substr($request->url(), 0, strpos($request->url(), $path));
			if (substr($currentUrl, strlen($currentUrl) - 1, strlen($currentUrl)) == '/') {
				$currentUrl = substr($currentUrl, 0, strlen($currentUrl) - 1);
			}
		}
		Debugbar::info("currentUrl = " . $currentUrl);

		$firstSegment = request()->segment(1);
		Debugbar::info("firstSegment = " . $firstSegment);

		$lastSegment = request()->segment(count(request()->segments()));
		Debugbar::info("lastSegment = " . $lastSegment);

		Debugbar::info("Path = " . $request->path());

		Debugbar::info("Path = " . $request->path());

		$routeName = Route::currentRouteName();
		Debugbar::info("Route name = " . $routeName);

		$segmentOneAndTrue = request()->segment(1) . "/" . request()->segment(2);
		Debugbar::info("SegmentOneAndTrue = " . $segmentOneAndTrue);

		$routeName = substr($routeName, strpos($routeName, '.') + 1, strlen($routeName));
		Debugbar::info("Route name substring = " . $routeName);

		$urlEn = $currentUrl . "/en" . $path;
		$urlVi = $currentUrl . "/vi" . $path;

		$urlEn = str_replace('//envi', '/en', str_replace('//enen', '/en', $urlEn));
		$urlVi = str_replace('//vien', '/vi', str_replace('//vivi', '/vi', $urlVi));

		View::share("urlEn", $urlEn);
		View::share("urlVi", $urlVi);
		View::share("currentUrl", $currentUrl);
		View::share("segmentOneAndTrue", $segmentOneAndTrue);

		Debugbar::info("Url en = " . $urlEn);

		Debugbar::info("Url vi = " . $urlVi);

		return $next($request);
	}
}
