<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Debugbar;
use Route;
use View;

class LoginAdminMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (Auth::check()) {

			Debugbar::info("Url = " . $request->url());

			$path = $request->path();
			$currentUrl = "";
			if ($path == '/') {
				$currentUrl = $request->url();
			} else {
				$currentUrl = substr($request->url(), 0, strpos($request->url(), $path));
				if (substr($currentUrl, strlen($currentUrl) - 1, strlen($currentUrl)) == '/') {
					$currentUrl = substr($currentUrl, 0, strlen($currentUrl) - 1);
				}
			}
			Debugbar::info("currentUrl = " . $currentUrl);

			$path = $request->path();

			Debugbar::info("Path = " . $request->path());

			Debugbar::info("Path = " . $request->path());

			$routeName = Route::currentRouteName();
			Debugbar::info("Route name = " . $routeName);

			$routeName = substr($routeName, strpos($routeName, '.') + 1, strlen($routeName));
			// $routeName = substr($path, 2, strlen($path));
			Debugbar::info("Route name substring = " . $routeName);

			$urlEn = route_local($routeName, [], true, 'en');
			$urlVi = route_local($routeName, [], true, 'vi');

			Debugbar::info("Url en = " . $urlEn);

			Debugbar::info("Url vi = " . $urlVi);

			View::share("urlEn", $urlEn);
			View::share("urlVi", $urlVi);
			View::share("currentUrl", $currentUrl);

			return $next($request);
		}
		return redirect()->route('login');
	}
}
