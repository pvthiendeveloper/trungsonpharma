<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class AddCategoryRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'categoryName' => 'required|max:100',
			'categoryNameEn' => 'required|max:100',
			'order' => 'required',
		];
	}

	/**
	 * Get custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'categoryName.required' => trans('messages.error_cate_name_empty'),
			'categoryName.max' => trans('messages.error_cate_name_max'),
			'categoryNameEn.required' => trans('messages.error_cate_name_empty_en'),
			'categoryNameEn.max' => trans('messages.error_cate_name_max'),
			'order.required' => trans('messages.error_cate_order_empty'),
		];
	}
}
