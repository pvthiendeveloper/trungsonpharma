<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class AddNewsRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'title' => 'required|max:100',
			'titleEn' => 'required|max:100',
			'order' => 'required',
		];
	}

	/**
	 * Get custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'title.required' => trans('messages.error_username_empty'),
			'title.max' => trans('messages.msg_title_max'),
			'titleEn.required' => trans('messages.msg_title_empty_en'),
			'titleEn.max' => trans('messages.msg_title_en_max'),
			'order.required' => trans('messages.error_cate_order_empty'),
		];
	}
}
