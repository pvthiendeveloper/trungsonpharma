<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'fullName' => 'required|max:100',
			'phoneNumber' => 'required|max:10',
			'email' => 'required|max:100',
			'zalo' => 'max:200',
			'facebook' => 'max:200',
			'message' => 'max:200',
		];
	}

	/**
	 * Get custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'fullName.required' => trans('messages.msg_error_fullname_empty'),
			'fullName.max' => trans('messages.msg_error_fullname_max'),
			'phoneNumber.required' => trans('messages.error_phone_number_empty'),
			'phoneNumber.max' => trans('messages.error_phone_number_max'),
			'email.required' => trans('messages.msg_error_email_empty'),
			'email.max' => trans('messages.msg_error_email_max'),
			'zalo.max' => trans('messages.error_zalo_max'),
			'facebook.max' => trans('messages.error_facebook_max'),
			'message.max' => trans('messages.error_facebook_max'),
		];
	}
}
