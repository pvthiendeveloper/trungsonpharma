<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditSettingsRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'introduceVideoUrl' => 'required',
		];
	}

	/**
	 * Get custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'introduceVideoUrl.required' => trans('messages.error_introduce_video_url_empty'),
		];
	}
}
