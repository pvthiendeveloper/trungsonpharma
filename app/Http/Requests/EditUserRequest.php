<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'username' => 'required|min:6|max:50',
			'fullname' => 'required|max:100',
			'email' => 'required|max:100',
		];
	}

	/**
	 * Get custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'username.required' => trans('messages.error_username_empty'),
			'username.min' => trans('messages.msg_error_username_min'),
			'username.max' => trans('messages.msg_error_username_max'),
			'fullname.required' => trans('messages.msg_error_fullname_empty'),
			'fullname.max' => trans('messages.msg_error_fullname_max'),
			'email.required' => trans('messages.msg_error_email_empty'),
			'email.max' => trans('messages.msg_error_email_max'),
			'password.required' => trans('messages.error_password_empty'),
			'password.min' => trans('messages.msg_error_password_min'),
			'password.max' => trans('messages.msg_error_password_max'),
			'password.confirmed' => trans('messages.msg_error_confirm_password_no_matching'),
			'password.password_confirmation' => trans('messages.msg_error_confirm_password_no_matching'),
			'password_confirmation.required' => trans('messages.msg_error_confirm_password_empty'),
			'password_confirmation.min' => trans('messages.msg_error_confirm_password_min'),
			'password_confirmation.max' => trans('messages.msg_error_confirm_password_max'),
		];
	}
}
