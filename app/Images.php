<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = "images";

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'image_url', 'image_type', 'status', 'is_deleted', 'created_at', 'updated_at',
	];

	public $timestamps = true;

	/**
	 * Get images by image type
	 *
	 * @var type = images type [ACTIVITY]
	 * @return array list images
	 */
	public static function getImagesByType($type) {
		$images = Images::all()
			->where('image_type', $type)
			->where('status', true)
			->where('is_deleted', false);
		return $images;
	}

	/**
	 * Get images by image type
	 *
	 * @var type = images type [ACTIVITY]
	 * @return array list images
	 */
	public static function getImages() {
		$images = Images::all()
			->where('is_deleted', false);
		return $images;
	}
}
