<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class News extends Model {
	protected $table = "news";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'title', 'title_en', 'alias', 'alias_en', 'short_description', 'short_description_en', 'content', 'content_en'
		, 'user_id', 'cate_id', 'order', 'image', 'status', 'is_deleted', 'created_at', 'updated_at',
	];

	public $timestamps = true;

	public function getTableName() {
		return $this->table;
	}

	public function getTitle() {
		return local_value($this->title, $this->title_en);
	}

	public function getShortDescription() {
		return local_value($this->short_description, $this->short_description_en);
	}

	public function getContent() {
		return local_value($this->content, $this->content_en);
	}

	public function getAlias() {
		return local_value($this->alias, $this->alias_en);
	}

	public function getUpdateDate() {
		if ($this->updated_at == null && $this->created_at == null) {
			return "";
		}
		$dateFormatEn = "d M y";

		if ($this->updated_at != null) {
			$dateFormatVi = date_format($this->updated_at, "d") . " tháng " . date_format($this->updated_at, "m") . " năm " . date_format($this->updated_at, "Y");

			// $dateVi = date_format($this->updated_at, $dateFormatVi);
			$dateEn = date_format($this->updated_at, $dateFormatEn);
			return local_value($dateFormatVi, $dateEn);
		}

		if ($this->created_at != null) {
			$dateFormatVi = date_format($this->created_at, "d") . " tháng " . date_format($this->created_at, "m") . " năm " . date_format($this->created_at, "Y");
			// $dateVi = date_format($this->created_at, $dateFormatVi);
			$dateEn = date_format($this->created_at, $dateFormatEn);
			return local_value($dateFormatVi, $dateEn);
		}
	}

	/**
	 * Get news by alias
	 *
	 * @var alias
	 * @return item news
	 */
	public static function getNewsByAlias($alias) {
		$news = News::where(function ($query) use ($alias) {
			return $query->where('alias', $alias)
				->orWhere('alias_en', $alias);
		})
			->where('status', true)
			->where('is_deleted', false)
			->first();
		return $news;
	}

	/**
	 * Get all news
	 *
	 * @return list news
	 */
	public static function getAll() {
		$news = News::all()
			->where('status', true)
			->where('is_deleted', false)
			->sortByDesc('created_at'); //sortByDesc
		return $news;
	}

	/**
	 * Get list new by cate id
	 *
	 * @return list news
	 */
	public static function getNewByCateId($cateId) {
		$categories = DB::table('categories')
			->where('parent_id', $cateId)
			->where('status', true)
			->where('is_deleted', false)
			->get();

		$cateIds = array();
		$cateIds[] = $cateId;
		foreach ($categories as $item) {
			$cateIds[] = $item->id;
		}

		// dd($cateIds);

		$news = DB::table('news')
			->join('categories', 'categories.id', '=', 'news.cate_id')
			->whereIn('cate_id', $cateIds)
			->get();
		dd($news);
		// $news = News::all()
		// 	->where('cate_id', $cateId)
		// 	->where('status', true)
		// 	->where('is_deleted', false)
		// 	->sortByDesc('created_at'); //sortByDesc
		return $news;
	}
}
