<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {
	protected $table = "settings";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id', 'introduce_video_url', 'status', 'created_at', 'updated_at',
	];

	public $timestamps = true;

	/**
	 * Get settings by cate id
	 *
	 * @var id = banner id, id default is 1
	 * @return item banner
	 */
	public static function getSettings($id) {
		$settings = Settings::all()
			->where('id', $id)
			->where('status', true)
			->first();
		return $settings;
	}
}
