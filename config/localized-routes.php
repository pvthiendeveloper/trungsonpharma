<?php

return [

	/**
	 * The locales you wish to support.
	 */
	'supported-locales' => explode(',', env('APP_LOCALES', 'vi,en')),

];
