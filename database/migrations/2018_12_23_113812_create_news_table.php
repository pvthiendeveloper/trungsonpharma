<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('news', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title', 100)->unique();
			$table->string('title_en', 100)->unique();
			$table->string('alias');
			$table->string('alias_en');
			$table->longText('short_description')->nullable();
			$table->longText('short_description_en')->nullable();
			$table->longText('content')->nullable();
			$table->longText('content_en')->nullable();
			$table->integer('user_id');
			$table->integer('cate_id');
			$table->integer('order');
			$table->string('image')->nullable();
			$table->string('image_2')->nullable();
			$table->boolean('status');
			$table->boolean('is_deleted');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('news');
	}
}
