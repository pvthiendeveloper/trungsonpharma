<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('banner', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title_1', 100)->nullable();
			$table->string('title_en_1', 100)->nullable();
			$table->string('link_1', 200)->nullable();
			$table->string('image_1', 200)->nullable();

			$table->string('title_2', 100)->nullable();
			$table->string('title_en_2', 100)->nullable();
			$table->string('link_2', 200)->nullable();
			$table->string('image_2', 200)->nullable();

			$table->string('title_3', 100)->nullable();
			$table->string('title_en_3', 100)->nullable();
			$table->string('link_3', 200)->nullable();
			$table->string('image_3', 200)->nullable();

			$table->string('title_4', 100)->nullable();
			$table->string('title_en_4', 100)->nullable();
			$table->string('link_4', 200)->nullable();
			$table->string('image_4', 200)->nullable();

			$table->string('title_5', 100)->nullable();
			$table->string('title_en_5', 100)->nullable();
			$table->string('link_5', 200)->nullable();
			$table->string('image_5', 200)->nullable();

			$table->string('title_6', 100)->nullable();
			$table->string('title_en_6', 100)->nullable();
			$table->string('link_6', 200)->nullable();
			$table->string('image_6', 200)->nullable();

			$table->boolean('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('banner');
	}
}
