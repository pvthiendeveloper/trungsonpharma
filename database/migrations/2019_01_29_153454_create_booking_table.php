<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('booking', function (Blueprint $table) {
			$table->increments('id');
			$table->string('full_name', 100);
			$table->string('phone_number', 100);
			$table->string('email', 100);
			$table->string('zalo', 200)->nullable();
			$table->string('facebook', 200)->nullable();
			$table->string('message', 200)->nullable();
			$table->string('status');
			$table->boolean('is_deleted');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('booking');
	}
}
