<?php

use Illuminate\Database\Seeder;

class BannerTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('banner')->delete();
		$faker = Faker\Factory::create();
		$banners = [
			[
				'title_1' => "Tiêu đề biểu ngữ 1", 'title_en_1' => "Banner title 1", 'link_1' => 'http://localhost:81/TrungSonPharma/vi/tin-tuc', 'image_1' => '/ckfinder/userfiles/images/cover-news.jpg',
				'title_2' => "Tiêu đề biểu ngữ 2", 'title_en_2' => "Banner title 2", 'link_2' => 'http://localhost:81/TrungSonPharma/vi/tin-tuc', 'image_2' => '/ckfinder/userfiles/images/cover-news.jpg',
				'title_3' => "Tiêu đề biểu ngữ 3", 'title_en_3' => "Banner title 3", 'link_3' => 'http://localhost:81/TrungSonPharma/vi/tin-tuc', 'image_3' => '/ckfinder/userfiles/images/cover-news.jpg',
				'title_4' => "Tiêu đề biểu ngữ 4", 'title_en_4' => "Banner title 4", 'link_4' => 'http://localhost:81/TrungSonPharma/vi/tin-tuc', 'image_4' => '/ckfinder/userfiles/images/cover-news.jpg',
				'title_5' => "Tiêu đề biểu ngữ 5", 'title_en_5' => "Banner title 5", 'link_5' => 'http://localhost:81/TrungSonPharma/vi/tin-tuc', 'image_5' => '/ckfinder/userfiles/images/cover-news.jpg',
				'title_6' => "Tiêu đề biểu ngữ 6", 'title_en_6' => "Banner title 6", 'link_6' => 'http://localhost:81/TrungSonPharma/vi/tin-tuc', 'image_6' => '/ckfinder/userfiles/images/cover-news.jpg',
				'status' => true,
			],
		];
		DB::table('banner')->insert($banners);
	}
}
