<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('categories')->delete();

		$categories = [
			['id' => 1, 'parent_id' => -1, 'name' => 'Trang chủ', 'name_en' => 'Home', 'alias' => 'trang-chu', 'alias_en' => 'home', 'order' => 1, 'status' => true, 'is_deleted' => false],
			['id' => 2, 'parent_id' => -1, 'name' => 'Bác sĩ', 'name_en' => 'Doctor', 'alias' => 'bac-si', 'alias_en' => 'doctor', 'order' => 2, 'status' => true, 'is_deleted' => false],
			['id' => 3, 'parent_id' => -1, 'name' => 'Dịch vụ', 'name_en' => 'Services', 'alias' => 'dich-vu', 'alias_en' => 'services', 'order' => 3, 'status' => true, 'is_deleted' => false],
			['id' => 4, 'parent_id' => -1, 'name' => 'Công nghệ', 'name_en' => 'Technology', 'alias' => 'cong-nghe', 'alias_en' => 'technology', 'order' => 4, 'status' => true, 'is_deleted' => false],
			['id' => 5, 'parent_id' => -1, 'name' => 'Tin tức', 'name_en' => 'News', 'alias' => 'tin-tuc', 'alias_en' => 'news', 'order' => 6, 'status' => true, 'is_deleted' => false],
			['id' => 6, 'parent_id' => -1, 'name' => 'Kiến thức làm đẹp', 'name_en' => 'Knowledge of beauty', 'alias' => 'kien-thuc-lam-dep', 'alias_en' => 'knowledge-of-beauty', 'order' => 5, 'status' => true, 'is_deleted' => false],
			['id' => 7, 'parent_id' => -1, 'name' => 'Tin mới', 'name_en' => 'News hot', 'alias' => 'tin-moi', 'alias_en' => 'news-hot', 'order' => 5, 'status' => true, 'is_deleted' => false],
			['id' => 8, 'parent_id' => 2, 'name' => 'Ths. Bs. Lý Quang Huy', 'name_en' => 'Dr. Huy', 'alias' => 'ths-bs-ly-quang-huy', 'alias_en' => 'dr-huy', 'order' => 7, 'status' => true, 'is_deleted' => false],
			['id' => 9, 'parent_id' => 2, 'name' => 'Ths. Bs. Nguyễn Hữu Giàu', 'name_en' => 'Dr. Giau', 'alias' => 'ths-bs-nguyen-huu-giau', 'alias_en' => 'dr-giau', 'order' => 8, 'status' => true, 'is_deleted' => false],
			['id' => 10, 'parent_id' => 2, 'name' => 'Bs. Trần Võ Thúy Vy', 'name_en' => 'Dr. Vy', 'alias' => 'bs-tran-vo-thuy-vy', 'alias_en' => 'dr-vy', 'order' => 9, 'status' => true, 'is_deleted' => false],
			['id' => 11, 'parent_id' => 3, 'name' => 'Điều trị sắc tố, trẻ hóa da', 'name_en' => 'Treatment of pigmentation, skin rejuvenation', 'alias' => 'dieu-tri-sac-to-tre-hoa-da', 'alias_en' => 'treatment-of-pigmentation-skin-rejuvenation', 'order' => 3, 'status' => true, 'is_deleted' => false],
			['id' => 12, 'parent_id' => 3, 'name' => 'Điều trị mụn', 'name_en' => 'Treatment of acne', 'alias' => 'dieu-tri-mun', 'alias_en' => 'treatment-of-acne', 'order' => 3, 'status' => true, 'is_deleted' => false],
			['id' => 13, 'parent_id' => 4, 'name' => 'Thẩm mỹ vùng mắt', 'name_en' => 'Aesthetic eye area', 'alias' => 'tham-my-vung-mat', 'alias_en' => 'aesthetic-eye-area', 'order' => 3, 'status' => true, 'is_deleted' => false],
			['id' => 14, 'parent_id' => 4, 'name' => 'Nâng mũi, tạo hình cằm V-line', 'name_en' => 'Nose lift, chin shape V-line', 'alias' => 'nang-mui-tao-hinh-cam-vline', 'alias_en' => 'nose-lift-chin-shape-v-line', 'order' => 3, 'status' => true, 'is_deleted' => false],
		];
		DB::table('categories')->insert($categories);
	}
}
