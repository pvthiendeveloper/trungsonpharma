<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('news')->delete();
		$faker = Faker\Factory::create();
		$limit = 9;
		for ($i = 0; $i < $limit; $i++) {
			DB::table('news')->insert([
				'title' => 'Tiêu đề tiếng việt ' . $i,
				'title_en' => $faker->sentence($nbWords = 6, $variableNbWords = true),
				'alias' => str_slug($faker->sentence($nbWords = 6, $variableNbWords = true)),
				'alias_en' => str_slug($faker->sentence($nbWords = 6, $variableNbWords = true)),
				'short_description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'short_description_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'user_id' => 1,
				'cate_id' => 1,
				'order' => 0,
				'image' => '/ckfinder/userfiles/images/cover-news.jpg',
				'image_2' => '/ckfinder/userfiles/images/image-3.jpg',
				'status' => true,
				'is_deleted' => false,
				'created_at' => '2019-01-20 15:52:49',
				'updated_at' => '2019-01-20 15:52:49',
			]);
		}
		$arrayServicesName = array('Điều trị sắc tố, trẻ hóa da', 'Điều trị sẹo', 'Tiểu phẫu thẩm mỹ', 'Triệt lông', 'Thẩm mỹ vùng mắt', 'Nâng mũi, tạo hình cằm V-line', 'Hút mỡ', 'Nâng ngực, thu gọn quầng vú, đầu vú, phẫu thuật đầu ngực tụt', 'Điều trị mồ hôi vùng nách');
		for ($i = 0; $i < $limit; $i++) {
			DB::table('news')->insert([
				'title' => $arrayServicesName[$i],
				'title_en' => $arrayServicesName[$i],
				'alias' => str_slug($arrayServicesName[$i]),
				'alias_en' => str_slug($arrayServicesName[$i]),
				'short_description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'short_description_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'user_id' => 1,
				'cate_id' => 3,
				'order' => 0,
				'image' => '/ckfinder/userfiles/images/cover-news.jpg',
				'image_2' => '/ckfinder/userfiles/images/image-3.jpg',
				'status' => true,
				'is_deleted' => false,
				'created_at' => '2019-01-20 15:52:49',
				'updated_at' => '2019-01-20 15:52:49',
			]);
		}

		for ($i = 0; $i < $limit; $i++) {
			DB::table('news')->insert([
				'title' => 'Tin tức ' . $i,
				'title_en' => 'news ' . $i,
				'alias' => 'tin-tuc-' . $i,
				'alias_en' => 'news-' . $i,
				'short_description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'short_description_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'user_id' => 1,
				'cate_id' => 5,
				'order' => 0,
				'image' => '/ckfinder/userfiles/images/cover-news.jpg',
				'image_2' => '/ckfinder/userfiles/images/image-3.jpg',
				'status' => true,
				'is_deleted' => false,
				'created_at' => '2019-01-20 15:52:49',
				'updated_at' => '2019-01-20 15:52:49',
			]);
		}

		for ($i = 0; $i < $limit; $i++) {
			DB::table('news')->insert([
				'title' => 'Điều trị sắc tố, trẻ hóa da ' . $i,
				'title_en' => 'Treatment of pigmentation, skin rejuvenation ' . $i,
				'alias' => 'dieu-tri-sac-to-tre-hoa-da' . $i,
				'alias_en' => 'treatment-of-pigmentation-skin-rejuvenation' . $i,
				'short_description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'short_description_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'user_id' => 1,
				'cate_id' => 11,
				'order' => 0,
				'image' => '/ckfinder/userfiles/images/cover-news.jpg',
				'image_2' => '/ckfinder/userfiles/images/image-3.jpg',
				'status' => true,
				'is_deleted' => false,
				'created_at' => '2019-01-20 15:52:49',
				'updated_at' => '2019-01-20 15:52:49',
			]);
		}

		for ($i = 0; $i < $limit; $i++) {
			DB::table('news')->insert([
				'title' => 'Tin hot ' . $i,
				'title_en' => 'News hot ' . $i,
				'alias' => 'tin-hot' . $i,
				'alias_en' => 'news-hot' . $i,
				'short_description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'short_description_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'content_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
				'user_id' => 1,
				'cate_id' => 7,
				'order' => 0,
				'image' => '/ckfinder/userfiles/images/cover-news.jpg',
				'image_2' => '/ckfinder/userfiles/images/image-3.jpg',
				'status' => true,
				'is_deleted' => false,
				'created_at' => '2019-01-20 15:52:49',
				'updated_at' => '2019-01-20 15:52:49',
			]);
		}

		DB::table('news')->insert([
			'title' => 'Điều trị mụn',
			'title_en' => 'Treatment of acne',
			'alias' => 'dieu-tri-mun',
			'alias_en' => 'treatment-of-acne',
			'short_description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
			'short_description_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
			'content' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
			'content_en' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
			'user_id' => 1,
			'cate_id' => 12,
			'order' => 0,
			'image' => '/ckfinder/userfiles/images/cover-news.jpg',
			'image_2' => '/ckfinder/userfiles/images/image-3.jpg',
			'status' => true,
			'is_deleted' => false,
			'created_at' => '2019-01-20 15:52:49',
			'updated_at' => '2019-01-20 15:52:49',
		]);

	}
}
