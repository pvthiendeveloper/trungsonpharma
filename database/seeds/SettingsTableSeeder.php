<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$settings = [
			['id' => 1, 'introduce_video_url' => "https://www.youtube.com/watch?time_continue=77&v=Cff2mbNWUIE", 'status' => true, 'is_deleted' => false],
		];

		DB::table('settings')->insert($settings);
	}
}
