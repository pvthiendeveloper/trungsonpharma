<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('users')->delete();
		$faker = Faker\Factory::create();
		DB::table('users')->insert([
			'name' => 'administrator',
			'username' => 'administrator',
			'password' => bcrypt('123456'),
			'email' => 'admin@gmail.com',
			'remember_token' => str_random(10),
			'role' => 1,
			'status' => true,
		]);
		$limit = 10;
		for ($i = 0; $i < $limit; $i++) {
			DB::table('users')->insert([
				'name' => $faker->name,
				'username' => $faker->username,
				'password' => bcrypt('123456'),
				'password' => Hash::make('123456'),
				'email' => $faker->unique()->email,
				'remember_token' => str_random(10),
				'role' => 2,
				'status' => false,
			]);
		}
	}
}
