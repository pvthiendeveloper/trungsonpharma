(function($) {
  showToast = function($title, $message, $type) {
    'use strict';
    var loaderBg = "";
    if($type == 'success'){
      loaderBg = '#f96868';
    }else if($type == 'info'){
     loaderBg = '#46c35f';
   }else if($type == 'warning'){
     loaderBg = '#57c7d4';
   }else if($type == 'danger'){
     loaderBg = '#f2a654';
   }

   resetToastPosition();
   $.toast({
    heading: $title,
    text: $message,
    showHideTransition: 'slide',
    icon: $type,
    loaderBg: loaderBg,
    position: 'top-right'
  })
 };
 resetToastPosition = function() {
    $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
    $(".jq-toast-wrap").css({
      "top": "",
      "left": "",
      "bottom": "",
      "right": ""
    }); //to remove previous position style
  }

  showSwalConfirm = function($url, $data, $type, $redirectUrl, $title, $msg, $textCancel) {
    swal({
      title: $title,
      text: $msg,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3f51b5',
      cancelButtonColor: '#ff4081',
      confirmButtonText: 'Great ',
      buttons: {
        cancel: {
          text: $textCancel,
          value: null,
          visible: true,
          className: "btn btn-danger",
          closeModal: true,
        },
        confirm: {
          text: 'OK',
          value: true,
          visible: true,
          className: "btn btn-primary",
          closeModal: true
        }
      }
    }).then((isConfirm) => {
      if (isConfirm){
       $.ajax({
        type: $type,
        url: $url,
        data: $data
      }).done(function(data) {
        if (data.error) {
          showSwalNoti($title, data.message);
        }else{
         showSwalSuccess($title, data.message, $redirectUrl);
       }
     }).fail(function(data) {
      alert('Lỗi: '.$data);
    });
   }
 });
  };

  showSwalNoti = function($title, $msg) {
   swal({
    title: $title,
    text: $msg,
    button: {
      text: "OK",
      value: true,
      visible: true,
      className: "btn btn-primary"
    }
  }).then((isConfirm) => {
    if (isConfirm){

    }
  });
};

showSwalSuccess = function($title, $msg, $redirectUrl) {
  swal({
    title: $title,
    text: $msg,
    icon: 'success',
    button: {
      text: "OK",
      value: true,
      visible: true,
      className: "btn btn-primary"
    }
  }).then((isConfirm) => {
    if (isConfirm){
      if ($redirectUrl != "") {
        window.location.href = $redirectUrl;
      }
    }
  });
};
})(jQuery);