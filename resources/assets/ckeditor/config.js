/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

 CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// filebrowserBrowseUrl: 'resources/assets/ckfinder/ckfinder.html',
	// filebrowserImageBrowseUrl: 'resources/assets/ckfinder/ckfinder.html?type=Images',
	// filebrowserFlashBrowseUrl: 'resources/assets/ckfinder/ckfinder.html?type=Flash',
	// filebrowserUploadUrl: 'resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	// filebrowserImageUploadUrl: 'resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	// filebrowserFlashUploadUrl: 'resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
};
