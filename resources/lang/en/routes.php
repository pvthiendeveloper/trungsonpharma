<?php

return [
	'login' => 'login',
	'loginAdmin' => 'login-admin',
	'lang' => 'en',
	'home' => 'home',
	'admin' => 'admin',
	'logout' => 'logout',
	'dashboard' => 'dashboard',
	'account' => 'account',
	'listAccount' => 'list-account',
	'addAccount' => 'add-account',
	'editAccount' => 'edit-account',
	'deleteAccount' => 'delete-account',
	'category' => 'category',
	'listCategory' => 'list-category',
	'addCategory' => 'add-category',
	'editCategory' => 'edit-category',
	'deleteCategory' => 'delete-category',
	'listNews' => 'list-news',
	'addNews' => 'add-news',
	'editNews' => 'edit-news',
	'deleteNews' => 'delete-news',
	'news' => 'news',
	'services' => 'services',
	'technology' => 'technology',
	'knowledgeOfBeauty' => 'knowledge-of-beauty',
	'booking' => 'booking',
	'drHuy' => 'dr-huy',
	'drGiau' => 'dr-giau',
	'drVi' => 'dr-vy',
	'doctor' => 'doctor',
	'bannerManagement' => 'banner-management',
	'listBooking' => 'list-booking',
	'deleteBooking' => 'delete-booking',
	'editBooking' => 'edit-booking',
	'webSettings' => 'web-settings',
	'listImages' => 'list-images',
	'images' => 'images',
	'addImages' => 'add-image',
	'deleteImages' => 'delete-image',
	'editImages' => 'edit-image',
];