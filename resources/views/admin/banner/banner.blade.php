@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_news') }}
@endsection
@section('script')
@parent
<script>
	$('#image-1').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#image-1").attr("src", file.getUrl());
		                $("#image-1-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});

	$('#image-2').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#image-2").attr("src", file.getUrl());
		                $("#image-2-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});

	$('#image-3').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#image-3").attr("src", file.getUrl());
		                $("#image-3-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});

	$('#image-4').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#image-4").attr("src", file.getUrl());
		                $("#image-4-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});

	$('#image-5').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#image-5").attr("src", file.getUrl());
		                $("#image-5-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});

	$('#image-6').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#image-6").attr("src", file.getUrl());
		                $("#image-6-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});

</script>
@endsection
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{ trans('messages.banner_management') }}</h4>
					<hr>
					<h5 class="card-description">{{ trans('messages.update_banner') }}</h5>
					<p class="card-description"><b>{{ trans('messages.note') }}:</b> {{ trans('messages.field_required') }}</p>
					@if(!empty($messageError))
					@component('admin.commond.alert')
					@slot('type')
					danger
					@endslot
					@slot('message')
					{{ $messageError }}
					@endslot
					@endcomponent
					@endif
					<form class="forms-sample" action="{{ route('bannerManagement') }}" method="POST">
						@csrf
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title')." 1" }}</label>
									<input type="text" class="form-control" name="title_1" value="{{ $banner->title_1 }}" placeholder="{{ trans('messages.title')." 1" }}">
									@if($errors->has('title_1'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_1') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title_en')." 1" }}</label>
									<input type="text" class="form-control" name="title_en_1" value="{{ $banner->title_en_1 }}" placeholder="{{ trans('messages.title_en')." 1" }}">
									@if($errors->has('title_en_1'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_en_1') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.link')." 1" }}</label>
									<input type="text" class="form-control" name="link_1" value="{{  $banner->link_1 }}" placeholder="{{ trans('messages.link')." 1" }}">
									@if($errors->has('link_1'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('link_1') }}
									</label>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.image') }} (1670x600) *</label>
									<input type="text" id="image-1-value" hidden="true" class="form-control" name="image_1" value="{{ $banner->image_1 }}">
									<img src="{{  empty($banner->image_1) ? "https://via.placeholder.com/1670x600" : $banner->image_1 }}" class="img-thumbnail" alt="image" id="image-1" style="border-radius: 0px" width="1670" height="600">
									@if($errors->has('image_1'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image_1') }}
									</label>
									@endif
								</div>
							</div>
						</div>
						<hr  width="100%" align="left" />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title')." 2" }}</label>
									<input type="text" class="form-control" name="title_2" value="{{ $banner->title_2 }}" placeholder="{{ trans('messages.title')." 2" }}">
									@if($errors->has('title_2'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_2') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title_en')." 2" }}</label>
									<input type="text" class="form-control" name="title_en_2" value="{{ $banner->title_en_2 }}" placeholder="{{ trans('messages.title_en')." 2" }}">
									@if($errors->has('title_en_2'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_en_2') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.link')." 2" }}</label>
									<input type="text" class="form-control" name="link_2" value="{{ $banner->link_2 }}" placeholder="{{ trans('messages.link')." 2" }}">
									@if($errors->has('link_2'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('link_2') }}
									</label>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.image') }} (1670x600)</label>
									<input type="text" id="image-2-value" hidden="true" class="form-control" name="image_2" value="{{ $banner->image_2 }}">
									<img src="{{  empty($banner->image_2) ? "https://via.placeholder.com/1670x600" : $banner->image_2 }}" class="img-thumbnail" alt="image" id="image-2" style="border-radius: 0px" width="1670" height="600">
									@if($errors->has('image_2'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image_2') }}
									</label>
									@endif
								</div>
							</div>
						</div>
						<hr  width="100%" align="left" />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title')." 3" }}</label>
									<input type="text" class="form-control" name="title_3" value="{{ $banner->title_3 }}" placeholder="{{ trans('messages.title')." 3" }}">
									@if($errors->has('title_3'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_3') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title_en')." 3" }}</label>
									<input type="text" class="form-control" name="title_en_3" value="{{ $banner->title_en_3 }}" placeholder="{{ trans('messages.title_en')." 3" }}">
									@if($errors->has('title_en_3'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_en_3') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.link')." 3" }}</label>
									<input type="text" class="form-control" name="link_3" value="{{ $banner->link_3 }}" placeholder="{{ trans('messages.link')." 3" }}">
									@if($errors->has('link_3'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('link_3') }}
									</label>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.image') }} (1670x600)</label>
									<input type="text" id="image-3-value" hidden="true" class="form-control" name="image_3" value="{{ $banner->image_3 }}">
									<img src="{{ empty($banner->image_3) ? "https://via.placeholder.com/1670x600" : $banner->image_3 }}" class="img-thumbnail" alt="image" id="image-3" style="border-radius: 0px" width="1670" height="600">
									@if($errors->has('image_3'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image_3') }}
									</label>
									@endif
								</div>
							</div>
						</div>
						<hr  width="100%" align="left" />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title')." 4" }}</label>
									<input type="text" class="form-control" name="title_4" value="{{ $banner->title_4 }}" placeholder="{{ trans('messages.title')." 4" }}">
									@if($errors->has('title_4'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_4') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title_en')." 4" }}</label>
									<input type="text" class="form-control" name="title_en_4" value="{{ $banner->title_en_4 }}" placeholder="{{ trans('messages.title_en')." 4" }}">
									@if($errors->has('title_en_4'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_en_4') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.link')." 4" }}</label>
									<input type="text" class="form-control" name="link_4" value="{{ $banner->link_4 }}" placeholder="{{ trans('messages.link')." 4" }}">
									@if($errors->has('link_4'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('link_4') }}
									</label>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.image') }} (1670x600)</label>
									<input type="text" id="image-4-value" hidden="true" class="form-control" name="image_4" value="{{ $banner->image_4 }}">
									<img src="{{ empty($banner->image_4) ? "https://via.placeholder.com/1670x600" : $banner->image_4 }}" class="img-thumbnail" alt="image" id="image-4" style="border-radius: 0px" width="1670" height="600">
									@if($errors->has('image_4'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image_4') }}
									</label>
									@endif
								</div>
							</div>
						</div>
						<hr  width="100%" align="left" />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title')." 5" }}</label>
									<input type="text" class="form-control" name="title_5" value="{{ $banner->title_5 }}" placeholder="{{ trans('messages.title')." 5" }}">
									@if($errors->has('title_5'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_5') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title_en')." 5" }}</label>
									<input type="text" class="form-control" name="title_en_5" value="{{ $banner->title_en_5 }}" placeholder="{{ trans('messages.title_en')." 5" }}">
									@if($errors->has('title_en_5'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_en_5') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.link')." 5" }}</label>
									<input type="text" class="form-control" name="link_5" value="{{ $banner->link_5 }}" placeholder="{{ trans('messages.link')." 5" }}">
									@if($errors->has('link_5'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('link_5') }}
									</label>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.image') }} (1670x600)</label>
									<input type="text" id="image-5-value" hidden="true" class="form-control" name="image_5" value="{{ $banner->image_5 }}">
									<img src="{{ empty($banner->image_5) ? "https://via.placeholder.com/1670x600" : $banner->image_5 }}" class="img-thumbnail" alt="image" id="image-5" style="border-radius: 0px" width="1670" height="600">
									@if($errors->has('image_5'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image_5') }}
									</label>
									@endif
								</div>
							</div>
						</div>
						<hr  width="100%" align="left" />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title')." 6" }}</label>
									<input type="text" class="form-control" name="title_6" value="{{ $banner->title_6 }}" placeholder="{{ trans('messages.title')." 6" }}">
									@if($errors->has('title_6'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_6') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.title_en')." 6" }}</label>
									<input type="text" class="form-control" name="title_en_6" value="{{ $banner->title_en_6 }}" placeholder="{{ trans('messages.title_en')." 6" }}">
									@if($errors->has('title_en_6'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title_en_6') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.link')." 6" }}</label>
									<input type="text" class="form-control" name="link_6" value="{{ $banner->link_6 }}" placeholder="{{ trans('messages.link')." 6" }}">
									@if($errors->has('link_6'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('link_6') }}
									</label>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputName1">{{ trans('messages.image') }} (1670x600)</label>
									<input type="text" id="image-6-value" hidden="true" class="form-control" name="image_6" value="{{ $banner->image_6 }}">
									<img src="{{ empty($banner->image_6) ? "https://via.placeholder.com/1670x600" : $banner->image_6 }}" class="img-thumbnail" alt="image" id="image-6" style="border-radius: 0px" width="1670" height="600">
									@if($errors->has('image_6'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image_6') }}
									</label>
									@endif
								</div>
							</div>
						</div>
						<hr  width="100%" align="left" />
						<button type="submit" class="btn btn-primary mr-2">{{ trans('messages.submit') }}</button>
						<a class="btn btn-light" href="{{ route('listAccount') }}" on>{{ trans('messages.cancel') }}</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection