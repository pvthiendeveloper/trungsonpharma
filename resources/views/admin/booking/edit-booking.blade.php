@extends('admin.layout.index')
@section('title')
{{ trans('messages.list_booking') }}
@endsection
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">{{ trans('messages.edit_booking') }}</h4>
          <hr>
          <h5 class="card-description">{{ trans('messages.edit_booking') }}</h5>
          <p class="card-description"><b>{{ trans('messages.note') }}:</b> {{ trans('messages.field_required') }}</p>
          @if(!empty($messageError))
          @component('admin.commond.alert')
          @slot('type')
          danger
          @endslot
          @slot('message')
          {{ $messageError }}
          @endslot
          @endcomponent
          @endif
          <form class="forms-sample" action="{{ route('editBooking').'-'.$booking->id }}" method="POST">
            @csrf
            <div class="form-group">
              <label for="exampleInputName1">{{ trans('messages.fullname') }} *</label>
              <input type="text" class="form-control" name="fullName" value="{{ $booking->full_name }}" placeholder="{{ trans('messages.fullname') }}">
              @if($errors->has('fullName'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('fullName') }}
              </label>
              @endif
            </div>
            <div class="form-group">
              <label for="exampleInputName1">{{ trans('messages.phone_number') }} *</label>
              <input type="text" class="form-control" name="phoneNumber" value="{{ $booking->phone_number }}" placeholder="{{ trans('messages.phone_number') }}">
              @if($errors->has('phoneNumber'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('phoneNumber') }}
              </label>
              @endif
            </div>
            <div class="form-group">
              <label for="exampleInputName1">Email *</label>
              <input type="text" class="form-control" name="email" value="{{ $booking->email }}" placeholder="Email">
              @if($errors->has('email'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('email') }}
              </label>
              @endif
            </div>
            <div class="form-group">
              <label for="exampleInputName1">Zalo</label>
              <input type="text" class="form-control" name="zalo" value="{{ $booking->zalo }}" placeholder="Zalo">
              @if($errors->has('zalo'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('zalo') }}
              </label>
              @endif
            </div>
            <div class="form-group">
              <label for="exampleInputName1">Facebook</label>
              <input type="text" class="form-control" name="facebook" value="{{ $booking->facebook }}" placeholder="Facebook">
              @if($errors->has('facebook'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('facebook') }}
              </label>
              @endif
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  @for ($index = 0; $index <= ($services->count()) / 2; $index++)
                  @php
                  $isChecked = "";
                  @endphp
                  @foreach ($bookingServices as $item)
                  @php
                  if($item->services_id == $services[$index]->id){
                   $isChecked = "checked";
                 }
                 @endphp
                 @endforeach
                 <div class="form-check">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" @php echo "".$isChecked; @endphp name="service[{{ $index }}]" value="{{ $services[$index]->id }}">
                    {{ $services[$index]->getTitle() }}
                  </label>
                </div>
                @endfor
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
               @for ($index = (int)(($services->count()) / 2 + 1); $index < $services->count(); $index++)
               @php
               $isChecked = "";
               @endphp
               @foreach ($bookingServices as $item)
               @php
               if($item->services_id == $services[$index]->id){
                 $isChecked = "checked";
               }
               @endphp
               @endforeach
               <div class="form-check">
                 <label class="form-check-label">
                  <input type="checkbox" class="form-check-input" @php echo "".$isChecked; @endphp name="service[{{ $index + (int)(($services->count()) / 2 + 1) }}]" value="{{ $services[$index]->id }}">
                  {{ $services[$index]->getTitle() }}
                </label>
              </div>
              @endfor
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleTextarea1">{{ trans('messages.message') }}</label>
          <textarea class="form-control" id="message" rows="10" name="message">{{ $booking->message }}</textarea>
          @if($errors->has('message'))
          <label id="cname-error" class="error mt-2 text-danger" for="cname">
            <i class="fa fa-exclamation-circle"></i> {{ $errors->first('message') }}
          </label>
          @endif
        </div>
        <div class="form-group">
          <label for="exampleInputName1">{{ trans('messages.status') }}</label>
          <input type="text" class="form-control" name="status" value="{{ $booking->status }}" placeholder="{{ trans('messages.status') }}">
          @if($errors->has('status'))
          <label id="cname-error" class="error mt-2 text-danger" for="cname">
            <i class="fa fa-exclamation-circle"></i> {{ $errors->first('status') }}
          </label>
          @endif
        </div>
        <button type="submit" class="btn btn-primary mr-2">{{ trans('messages.submit') }}</button>
        <a class="btn btn-light" href="{{ route('listBooking') }}" on>{{ trans('messages.cancel') }}</a>
      </form>
    </div>
  </div>
</div>
</div>
</div>
</div>

@endsection