@extends('admin.layout.index')
@section('title')
{{ trans('messages.list_booking') }}
@endsection
@section('script')
@parent
<script src="{{ asset('resources/assets/admin/js/data-table.js') }}"></script>
@endsection
@if(!empty(Session::get('toastData')))
@component('admin.commond.toast')
@slot('title')
{{ Session::get('toastData')['title'] }}
@endslot
@slot('message')
{{ Session::get('toastData')['message'] }}
@endslot
@slot('type')
{{ Session::get('toastData')['type'] }}
@endslot
@endcomponent
@endif
@section('content')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">{{ trans('messages.list_booking') }}</h4>
      <div class="row">
        <div class="col-12 table-responsive">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>ID #</th>
                <th>{{ trans('messages.fullname') }}</th>
                <th>{{ trans('messages.phone_number') }}</th>
                <th>Email</th>
                <th>Zalo</th>
                <th>Facebook</th>
                <th>{{ trans('messages.status') }}</th>
                <th>{{ trans('messages.actions') }}</th>
              </tr>
            </thead>
            <tbody>
              @if (empty($booking))
              Empty
              @else
              @foreach ($booking as $item)
              <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->full_name }}</td>
                <td>{{ $item->phone_number }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->zalo }}</td>
                <td>{{ $item->facebook }}</td>
                <td>{{ $item->status }}</td>
                <td>
                  <a href="{{ route('editBooking').'-'.$item->id}}">
                    <button type="button" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.edit') }}"><i class="ti-pencil"></i></button>
                  </a>
                  <button type="button" onclick="showSwalConfirm('{{ route('deleteBooking').'-'.$item->id}}', '', 'GET', '{{ route('listBooking') }}', '{{ trans('messages.notification') }}', '{{ trans('messages.msg_delete_confirm') }}',
                  '{{ trans('messages.cancel') }}')" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.delete') }}"><i class="ti-trash"></i></button>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection