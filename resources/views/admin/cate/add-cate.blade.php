@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_category') }}
@endsection
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">{{ trans('messages.manager_category') }}</h4>
          <hr>
          <h5 class="card-description">{{ trans('messages.add_category') }}</h5>
          <p class="card-description"><b>{{ trans('messages.note') }}:</b> {{ trans('messages.field_required') }}</p>
          @if(!empty($messageError))
          @component('admin.commond.alert')
          @slot('type')
          danger
          @endslot
          @slot('message')
          {{ $messageError }}
          @endslot
          @endcomponent
          @endif
          <form class="forms-sample" action="{{ route('addCategory') }}" method="POST">
            @csrf
            <div class="form-group">
              <label for="exampleInputName1">{{ trans('messages.category_name') }} *</label>
              <input type="text" class="form-control" name="categoryName" value="{{ old('categoryName') }}" placeholder="{{ trans('messages.category_name') }}">
              @if($errors->has('categoryName'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('categoryName') }}
              </label>
              @endif
            </div>
            <div class="form-group">
              <label for="exampleInputName1">{{ trans('messages.category_name_en') }} *</label>
              <input type="text" class="form-control" name="categoryNameEn" value="{{ old('categoryNameEn') }}" placeholder="{{ trans('messages.category_name_en') }}">
              @if($errors->has('categoryNameEn'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('categoryNameEn') }}
              </label>
              @endif
            </div>
            <div class="form-group">
              <label for="exampleSelectGender">{{ trans('messages.category_parent') }} *</label>
              <select class="form-control" name="parentId">
                <option value="-1">{{ trans('messages.unknown') }}</option>
                @if (!empty($categories))
                @foreach ($categories as $item)
                <option value="{{ $item->id }}">
                  @if (trans('messages.lang') == 'vi')
                  {{ $item->name }}
                  @else
                  {{ $item->name_en }}
                  @endif
                </option>
                @endforeach
                @endif
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputName1">{{ trans('messages.category_order') }} *</label>
              <input type="number" class="form-control"" name="order" value="{{ old('order') }}" placeholder="{{ trans('messages.category_order') }}">
              @if($errors->has('order'))
              <label id="cname-error" class="error mt-2 text-danger" for="cname">
                <i class="fa fa-exclamation-circle"></i> {{ $errors->first('order') }}
              </label>
              @endif
            </div>
            <div class="form-group">
              <label for="exampleSelectGender">{{ trans('messages.status') }} *</label>
              <select class="form-control" id="exampleSelectGender" name="status">
                <option value="1" {{ (old('status') == '1' ? "selected":"") }}>{{ trans('messages.active') }}</option>
                <option value="0" {{ (old('status') == '0' ? "selected":"") }}>{{ trans('messages.block') }}</option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary mr-2">{{ trans('messages.submit') }}</button>
            <a class="btn btn-light" href="{{ route('listAccount') }}" on>{{ trans('messages.cancel') }}</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

@endsection