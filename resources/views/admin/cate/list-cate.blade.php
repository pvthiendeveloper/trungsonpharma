@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_category') }}
@endsection
@section('script')
@parent
<script src="{{ asset('resources/assets/admin/js/data-table.js') }}"></script>
@endsection
@if(!empty(Session::get('toastData')))
@component('admin.commond.toast')
@slot('title')
{{ Session::get('toastData')['title'] }}
@endslot
@slot('message')
{{ Session::get('toastData')['message'] }}
@endslot
@slot('type')
{{ Session::get('toastData')['type'] }}
@endslot
@endcomponent
@endif
@section('content')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">{{ trans('messages.manager_category') }}</h4>
      <div class="row">
        <div class="col-12 table-responsive">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>ID #</th>
                <th>{{ trans('messages.category_name') }}</th>
                <th>{{ trans('messages.category_name_en') }}</th>
                <th>{{ trans('messages.category_parent') }}</th>
                <th>{{ trans('messages.category_order') }}</th>
                <th>{{ trans('messages.status') }}</th>
                <th>{{ trans('messages.actions') }}</th>
              </tr>
            </thead>
            <tbody>
              @if (empty($categories))
              Empty
              @else
              @foreach ($categories as $item)
              <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->name_en }}</td>
                <td>{{ $item->parent_id }}</td>
                <td>{{ $item->order }}</td>
                <td>
                  @if ($item->status == 1)
                  <label class="badge badge-success">{{ trans('messages.active') }}</label>
                  @else
                  <label class="badge badge-danger">{{ trans('messages.block') }}</label>
                  @endif
                </td>
                <td>
                  @if ($item->parent_id != -1 && $item->parent_id != 2)
                  <a href="{{ route('editCategory').'-'.$item->id}}">
                    <button type="button" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.edit') }}"><i class="ti-pencil"></i></button>
                  </a>
                  <button type="button" onclick="showSwalConfirm('{{ route('deleteCategory').'-'.$item->id}}', '', 'GET', '{{ route('listCategory') }}', '{{ trans('messages.notification') }}', '{{ trans('messages.msg_delete_confirm') }}',
                  '{{ trans('messages.cancel') }}')" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.delete') }}"><i class="ti-trash"></i>
                </button>
                @endif
              </td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection