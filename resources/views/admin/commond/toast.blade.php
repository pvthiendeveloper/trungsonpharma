@section('script')
@parent
<script type="text/javascript">
  showToast('{{ $title }}', '{{ $message }}', '{{ $type }}');
</script>
@endsection
