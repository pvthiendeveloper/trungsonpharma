@extends('admin.layout.index')
@section('title')
{{ trans('messages.home') }}
@endsection
@section('content')
<!-- partial -->
<div class="container-fluid page-body-wrapper">
	<div class="main-panel">
		<div class="content-wrapper">
			<div class="row">
				<div class="col-12 grid-margin">
					<div class="card card-statistics">
						<div class="card-body p-0">
							<div class="row">
								<div class="col-md-6 col-lg-3">
									<div class="d-flex justify-content-between border-right card-statistics-item">
										<div>
											<h1>{{ $toalAccount }}</h1>
											<p class="text-muted mb-0">{{ trans('messages.total_accounts') }}</p>
										</div>
										<i class="icon-people text-primary icon-lg"></i>
									</div>
								</div>
								<div class="col-md-6 col-lg-3">
									<div class="d-flex justify-content-between border-right card-statistics-item">
										<div>
											<h1>{{ $totalCate }}</h1>
											<p class="text-muted mb-0">{{ trans('messages.total_cate') }}</p>
										</div>
										<i class="icon-layers text-primary icon-lg"></i>
									</div>
								</div>
								<div class="col-md-6 col-lg-3">
									<div class="d-flex justify-content-between border-right card-statistics-item">
										<div>
											<h1>{{ $totalNews }}</h1>
											<p class="text-muted mb-0">{{ trans('messages.total_news') }}</p>
										</div>
										<i class="icon-layers text-primary icon-lg"></i>
									</div>
								</div>
								<div class="col-md-6 col-lg-3">
									<div class="d-flex justify-content-between card-statistics-item">
										<div>
											<h1>{{ $totalBooking }}</h1>
											<p class="text-muted mb-0">{{ trans('messages.total_booking') }}</p>
										</div>
										<i class="icon-refresh text-primary icon-lg"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- content-wrapper ends -->
	</div>
	<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
@endsection