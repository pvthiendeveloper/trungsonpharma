@extends('admin.layout.index')
@section('title')
{{ trans('messages.add_image') }}
@endsection
@section('script')
@parent
<script>
	$('#news-image').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#news-image").attr("src", file.getUrl());
		                $("#news-image-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});
</script>
@endsection
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{ trans('messages.manager_doctor') }}</h4>
					<hr>
					<h5 class="card-description">{{ trans('messages.add_image') }}</h5>
					<p class="card-description"><b>{{ trans('messages.note') }}:</b> {{ trans('messages.field_required') }}</p>
					@if(!empty($messageError))
					@component('admin.commond.alert')
					@slot('type')
					danger
					@endslot
					@slot('message')
					{{ $messageError }}
					@endslot
					@endcomponent
					@endif
					<form class="forms-sample" action="{{ route('addImages') }}" method="POST">
						@csrf
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.image') }} (710x470) *</label>
							<input type="text" id="news-image-value" hidden="true" class="form-control" name="image" value="{{ old('image') }}">
							<img src="https://via.placeholder.com/710x470" class="img-thumbnail" alt="image" id="news-image" style="border-radius: 0px" width="710" height="470">
							@if($errors->has('image'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleSelectGender">{{ trans('messages.status') }} *</label>
							<select class="form-control" id="exampleSelectGender" name="status">
								<option value="1" {{ (old('status') == '1' ? "selected":"") }}>{{ trans('messages.active') }}</option>
								<option value="0" {{ (old('status') == '0' ? "selected":"") }}>{{ trans('messages.block') }}</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary mr-2">{{ trans('messages.submit') }}</button>
						<a class="btn btn-light" href="{{ route('listImages') }}" on>{{ trans('messages.cancel') }}</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection