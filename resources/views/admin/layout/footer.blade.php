 <footer class="footer">
 	<div class="w-100 clearfix">
 		<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">{{ trans('messages.copyright') }}</span>
 	</div>
 </footer>