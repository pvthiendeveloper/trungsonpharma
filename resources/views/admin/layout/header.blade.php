<nav class="navbar horizontal-layout col-lg-12 col-12 p-0">
	<div class="nav-top flex-grow-1">
		<div class="container d-flex flex-row h-100 align-items-center">
			<div class="text-center navbar-brand-wrapper d-flex align-items-center">
				<a class="navbar-brand brand-logo" href="{{ route('dashboard') }}"><img src="{{ asset('resources/assets/client/images/logo.png') }}" alt="logo"/></a>
				<a class="navbar-brand brand-logo-mini" href="{{ route('dashboard') }}"><img src="{{ asset('resources/assets/client/images/logo.png') }}" alt="logo"/></a>
			</div>
			<div class="navbar-menu-wrapper d-flex align-items-center justify-content-between flex-grow-1">
				<form class="search-field d-none d-md-flex" action="#">
					<div class="form-group mb-0">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="icon-magnifier"></i></span>
							</div>
							<input type="text" class="form-control" placeholder="{{ trans('messages.search_here') }}">
						</div>
					</div>
				</form>
				<ul class="navbar-nav navbar-nav-right mr-0 ml-auto">
					<li class="nav-item dropdown d-none d-lg-flex nav-language">
						<div class="nav-link">
							<span class="dropdown-toggle btn btn-sm" id="languageDropdown" data-toggle="dropdown">
								@if (trans('messages.lang') == 'en')
									<i class="flag-icon flag-icon-us ml-2"></i>
								@else
									<i class="flag-icon flag-icon-vn ml-2"></i>
								@endif
								{{ trans('messages.current_language') }}
							</span>
							<div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
								<a class="dropdown-item font-weight-medium" href="{{ $urlEn }}">
									<i class="flag-icon flag-icon-us ml-2">&#32;</i>
									{{ trans('messages.english') }}
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item font-weight-medium" href="{{ $urlVi }}">
									<i class="flag-icon flag-icon-vn ml-2">&#32;</i>
									{{ trans('messages.vietnamse') }}
								</a>
							</div>
						</div>
					</li>
					<li class="nav-item nav-profile dropdown">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
							<span class="nav-profile-name">{{ Auth::user()->name }}</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
							<a class="dropdown-item" href="{{ route('logout') }}">
								<i class="icon-logout text-primary mr-2"></i>
								{{ trans('messages.logout') }}
							</a>
						</div>
					</li>
				</ul>
				<button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
					<span class="icon-menu text-dark"></span>
				</button>
			</div>
		</div>
	</div>
	@include('admin.layout.menu')
</nav>