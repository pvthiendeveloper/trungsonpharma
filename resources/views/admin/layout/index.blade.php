<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/vendors/css/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/vendors/css/vendor.bundle.addons.css') }}">
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/vendors/css/vendor.bundle.addons.css') }}">
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/vendors/iconfonts/ti-icons/css/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/vendors/iconfonts/font-awesome/css/font-awesome.min.css') }}">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('resources/assets/admin/images/logo.png') }}" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_horizontal-navbar.html -->
    @include('admin.layout.header')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="main-panel">
        @yield('content')
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        @include('admin.layout.footer')
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{ asset('resources/assets/admin/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('resources/assets/admin/vendors/js/vendor.bundle.addons.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('resources/assets/admin/js/template.js') }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{ asset('resources/assets/admin/js/dashboard.js') }}"></script>
  <script src="{{ asset('resources/assets/admin/js/todolist.js') }}"></script>
  <script src="{{ asset('resources/assets/admin/js/tooltips.js') }}"></script>
  <script src="{{ asset('resources/assets/admin/js/admin-js.js') }}"></script>
  <script src="{{ asset('resources/assets/ckeditor/ckeditor.js') }}"></script>
  <script src="{{ asset('resources/assets/ckfinder/ckfinder.js') }}"></script>
  <!-- End custom js for this page-->
   @yield('script')
</body>

</html>
