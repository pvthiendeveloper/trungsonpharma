<div class="nav-bottom">
	<div class="container">
		<ul class="nav page-navigation">
			<li class="nav-item">
				<a href="{{ route('dashboard') }}" class="nav-link"><i class="link-icon icon-screen-desktop"></i><span class="menu-title">{{ trans('messages.dashboard') }}</span></a>
			</li>
			<li class="nav-item">
				<a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">{{ trans('messages.manager_account') }}</span><i class="menu-arrow"></i></a>
				<div class="submenu">
					<ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="{{ route('listAccount') }}">{{ trans('messages.list_account') }}</a></li>
						<li class="nav-item"><a class="nav-link" href="{{ route('addAccount') }}">{{ trans('messages.add_account') }}</a></li>
					</ul>
				</div>
			</li>

			<li class="nav-item">
				<a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">{{ trans('messages.manager_category') }}</span><i class="menu-arrow"></i></a>
				<div class="submenu">
					<ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="{{ route('listCategory') }}">{{ trans('messages.list_category') }}</a></li>
						<li class="nav-item"><a class="nav-link" href="{{ route('addCategory') }}">{{ trans('messages.add_category') }}</a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item">
				<a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">{{ trans('messages.manager_news') }}</span><i class="menu-arrow"></i></a>
				<div class="submenu">
					<ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="{{ route('listNews') }}">{{ trans('messages.list_news') }}</a></li>
						<li class="nav-item"><a class="nav-link" href="{{ route('addNews') }}">{{ trans('messages.add_news') }}</a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item">
				<a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">{{ trans('messages.booking_management') }}</span><i class="menu-arrow"></i></a>
				<div class="submenu">
					<ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="{{ route('listBooking') }}">{{ trans('messages.list_booking') }}</a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item mega-menu">
				<a href="javascript:void(0)" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">{{ trans('messages.order_manament') }}</span><i class="menu-arrow"></i></a>
				<div class="submenu">
					<div class="col-group-wrapper row">
							<div class="col-group col-md-3">
								<p class="category-heading">{{ trans('messages.manager_doctor') }}</p>
								<ul class="submenu-item">
									<li class="nav-item">
										<a class="nav-link" href="{{ route('addImages') }}">{{ trans('messages.add_image') }}</a>
										<a class="nav-link" href="{{ route('listImages') }}">{{ trans('messages.manager_activity_images') }}</a>
									</li>
								</ul>
							</div>
							<div class="col-group col-md-3">
								<p class="category-heading">{{ trans('messages.order_manament') }}</p>
								<ul class="submenu-item">
									<li class="nav-item">
										<a class="nav-link" href="{{ route('bannerManagement') }}">{{ trans('messages.banner_manament') }}</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="{{ route('webSettings') }}">{{ trans('messages.web_settings') }}</a>
									</li>
								</ul>
							</div>
						</div>
				</div>
				{{-- <div class="submenu">
						<div class="col-group-wrapper row">
							<div class="col-group col-md-3">
								<p class="category-heading">{{ trans('messages.manager_doctor') }}</p>
								<ul class="submenu-item">
									<li class="nav-item">
										<a class="nav-link" href="{{ route('listImages') }}">{{ trans('messages.manager_activity_images') }}</a>
									</li>
								</ul>
							</div>
						</div>
					</div> --}}
				</li>
			</ul>
		</div>
	</div>