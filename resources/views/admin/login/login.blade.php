<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>{{ trans('messages.login') }}</title>
	<!-- plugins:css -->
	<link rel="stylesheet" href="../resources/assets/admin/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
	<link rel="stylesheet" href="../resources/assets/admin/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
	<link rel="stylesheet" href="../resources/assets/admin/vendors/css/vendor.bundle.base.css">
	<link rel="stylesheet" href="../resources/assets/admin/vendors/css/vendor.bundle.addons.css">
	<link rel="stylesheet" href="../resources/assets/admin/vendors/iconfonts/font-awesome/css/font-awesome.min.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="../resources/assets/admin/css/style.css">
	<link rel="stylesheet" href="../resources/assets/admin/css/admin-style.css">
	<link rel=" text/plain" href="../resources/assets/admin/css/admin-style.scss">
	<!-- endinject -->
	<link rel="shortcut icon" href="{{ asset('resources/assets/admin/images/logo.png') }}"/>
</head>
<body>
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper">
			<div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
				<div class="row w-100 mx-auto">
					<div class="col-lg-3 mx-auto">
						<div class="auto-form-wrapper">
							<div class="text-block text-center my-3">
								<span class="text font-weight-semibold">{{ trans('messages.login') }}</span>
							</div>
							@if(!empty($messageError))
							@component('admin.commond.alert')
							@slot('type')
							danger
							@endslot
							@slot('message')
							{{ $messageError }}
							@endslot
							@endcomponent
							@endif
							<form action="{{ route('login') }}" method="POST">
								@csrf
								<div class="form-group">
									<label class="label">{{ trans('messages.username') }} *</label>
									<div class="input-group">
										<input type="emal" name="username" value="{{ old('username') }}" class="form-control" placeholder="{{ trans('messages.username') }}">
										<div class="input-group-append">
											<span class="input-group-text">
											</span>
										</div>
									</div>
									@if($errors->has('username'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('username') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<label class="label">{{ trans('messages.password') }} *</label>
									<div class="input-group">
										<input type="password" name="password" value="{{ old('password') }}" class="form-control" placeholder="*********">
										<div class="input-group-append">
											<span class="input-group-text">
											</span>
										</div>
									</div>
									@if($errors->has('password'))
									<label id="cname-error" class="error mt-2 text-danger" for="cname">
										<i class="fa fa-exclamation-circle"></i> {{ $errors->first('password') }}
									</label>
									@endif
								</div>
								<div class="form-group">
									<button class="btn btn-primary submit-btn btn-block">{{ trans('messages.login') }}</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- content-wrapper ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->
	<!-- plugins:js -->
	<script src="../resources/assets/admin/vendors/js/vendor.bundle.base.js"></script>
	<script src="../resources/assets/admin/vendors/js/vendor.bundle.addons.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script src="../resources/assets/admin/js/template.js"></script>
	<!-- endinject -->
</body>

</html>
