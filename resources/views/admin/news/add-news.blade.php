@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_news') }}
@endsection
@section('script')
@parent
<script>
	/*CKEDITOR.replace('short-description', {
		filebrowserBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html') }}',
		filebrowserImageBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Images') }}',
		filebrowserFlashBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Flash') }}',
		filebrowserUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
		filebrowserImageUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
		filebrowserFlashUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
		height: '400px',
	});*/

	/*CKEDITOR.replace('short-description-en', {
		filebrowserBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html') }}',
		filebrowserImageBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Images') }}',
		filebrowserFlashBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Flash') }}',
		filebrowserUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
		filebrowserImageUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
		filebrowserFlashUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
		height: '400px',
	});*/

	CKEDITOR.replace('content', {
		filebrowserBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html') }}',
		filebrowserImageBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Images') }}',
		filebrowserFlashBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Flash') }}',
		filebrowserUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
		filebrowserImageUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
		filebrowserFlashUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
		height: '800px',
	});

	CKEDITOR.replace('content-en', {
		filebrowserBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html') }}',
		filebrowserImageBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Images') }}',
		filebrowserFlashBrowseUrl: '{{ asset('resources/assets/ckfinder/ckfinder.html?type=Flash') }}',
		filebrowserUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
		filebrowserImageUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
		filebrowserFlashUploadUrl: '{{ asset('resources/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
		height: '800px',
	});

	$('#news-image').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#news-image").attr("src", file.getUrl());
		                $("#news-image-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});



	$('#news-image-2').click(function () {
		CKFinder.popup( {
		        // Enable file choose mechanism.
		        chooseFiles: true,
		        // Restrict user to choose only from Images resource type.
		        resourceType: 'Images',
		        // Add handler for events that are fired when user select's file.
		        onInit: function( finder ) {
		            // User selects original image.
		            finder.on( 'files:choose', function( evt ) {
		                // Get first file because user might select multiple files
		                var file = evt.data.files.first();
		                $("#news-image-2").attr("src", file.getUrl());
		                $("#news-image-2-value").attr("value", file.getUrl());
		            } );

		            // User selects resized image.
		            finder.on( 'file:choose:resizedImage', function( evt ) {
		            	showUploadedImage( evt.data.resizedUrl );
		            } );
		        }
		    });
	});

</script>
@endsection
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{ trans('messages.manager_news') }}</h4>
					<hr>
					<h5 class="card-description">{{ trans('messages.add_news') }}</h5>
					<p class="card-description"><b>{{ trans('messages.note') }}:</b> {{ trans('messages.field_required') }}</p>
					@if(!empty($messageError))
					@component('admin.commond.alert')
					@slot('type')
					danger
					@endslot
					@slot('message')
					{{ $messageError }}
					@endslot
					@endcomponent
					@endif
					<form class="forms-sample" action="{{ route('addNews') }}" method="POST">
						@csrf
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.title') }} *</label>
							<input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="{{ trans('messages.title') }}">
							@if($errors->has('title'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('title') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.title_en') }} *</label>
							<input type="text" class="form-control" name="titleEn" value="{{ old('titleEn') }}" placeholder="{{ trans('messages.title_en') }}">
							@if($errors->has('titleEn'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('titleEn') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleSelectGender">{{ trans('messages.category') }} *</label>
							<select class="form-control" name="cateId">
								<option value="-1">{{ trans('messages.unknown') }}</option>
								@if (!empty($categories))
								@foreach ($categories as $item)
								<option value="{{ $item->id }}">
									@if (trans('messages.lang') == 'vi')
									{{ $item->name }}
									@else
									{{ $item->name_en }}
									@endif
								</option>
								@endforeach
								@endif
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.order') }} *</label>
							<input type="number" class="form-control" name="order" value="{{ old('order') }}" placeholder="{{ trans('messages.order') }}">
							@if($errors->has('order'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('order') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.image') }} (400x250)</label>
							<input type="text" id="news-image-value" hidden="true" class="form-control" name="image" value="{{ old('image') }}">
							<img src="https://via.placeholder.com/400x250" class="img-thumbnail" alt="image" id="news-image" style="border-radius: 0px" width="400" height="250">
							@if($errors->has('image'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.image') }} (450x550)</label>
							<input type="text" id="news-image-2-value" hidden="true" class="form-control" name="image_2" value="{{ old('image_2') }}">
							<img src="https://via.placeholder.com/450x550" class="img-thumbnail" alt="image" id="news-image-2" style="border-radius: 0px" width="450" height="550">
							@if($errors->has('image_2'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('image_2') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleTextarea1">{{ trans('messages.short_description') }}</label>
							<textarea class="form-control" id="short-description" rows="8" name="shortDescription">{{ old('shortDescription') }}</textarea>
						</div>
						<div class="form-group">
							<label for="exampleTextarea1">{{ trans('messages.short_description_en') }}</label>
							<textarea class="form-control" id="short-description-en" rows="8" name="shortDescriptionEn">{{ old('shortDescriptionEn') }}</textarea>
						</div>
						<div class="form-group">
							<label for="exampleTextarea1">{{ trans('messages.content') }}</label>
							<textarea class="form-control" id="content" rows="10" name="content">{{ old('content') }}</textarea>
						</div>
						<div class="form-group">
							<label for="exampleTextarea1">{{ trans('messages.content_en') }}</label>
							<textarea class="form-control" id="content-en" rows="10" name="contentEn">{{ old('contentEn') }}</textarea>
						</div>
						<div class="form-group">
							<label for="exampleSelectGender">{{ trans('messages.status') }} *</label>
							<select class="form-control" id="exampleSelectGender" name="status">
								<option value="1" {{ (old('status') == '1' ? "selected":"") }}>{{ trans('messages.active') }}</option>
								<option value="0" {{ (old('status') == '0' ? "selected":"") }}>{{ trans('messages.block') }}</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary mr-2">{{ trans('messages.submit') }}</button>
						<a class="btn btn-light" href="{{ route('listAccount') }}" on>{{ trans('messages.cancel') }}</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection