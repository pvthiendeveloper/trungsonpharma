@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_news') }}
@endsection
@section('script')
@parent
<script src="{{ asset('resources/assets/admin/js/data-table.js') }}"></script>
@endsection
@if(!empty(Session::get('toastData')))
@component('admin.commond.toast')
@slot('title')
{{ Session::get('toastData')['title'] }}
@endslot
@slot('message')
{{ Session::get('toastData')['message'] }}
@endslot
@slot('type')
{{ Session::get('toastData')['type'] }}
@endslot
@endcomponent
@endif
@section('content')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">{{ trans('messages.list_news') }}</h4>
      <div class="row">
        <div class="col-12 table-responsive">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>ID #</th>
                <th>{{ trans('messages.image') }}</th>
                <th style="width: 5%">{{ trans('messages.title') }}</th>
                <th>{{ trans('messages.order') }}</th>
                <th>{{ trans('messages.status') }}</th>
                <th>{{ trans('messages.actions') }}</th>
              </tr>
            </thead>
            <tbody>
              @if (empty($news))
              Empty
              @else
              @foreach ($news as $item)
              <tr>
                <td>{{ $item->id }}</td>
                <td>
                  <img src="{{ $item->image }}"  class="img-thumbnail" alt="image" width="70px" height="70px" style="    width: 70px;
    height: 70px;
    border-radius: 0%;">
                </td>
                <td>
                  @if (trans('messages.lang') == 'vi')
                  {{ $item->title }}
                  @else
                  {{ $item->title_en }}
                  @endif
                </td>
                <td>{{ $item->order }}</td>
                <td>
                  @if ($item->status == 1)
                  <label class="badge badge-success">{{ trans('messages.active') }}</label>
                  @else
                  <label class="badge badge-danger">{{ trans('messages.block') }}</label>
                  @endif
                </td>
                <td>
                  <a href="{{ route('editNews').'-'.$item->id}}">
                    <button type="button" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.edit') }}"><i class="ti-pencil"></i></button>
                  </a>
                  <button type="button" onclick="showSwalConfirm('{{ route('deleteNews').'-'.$item->id}}', '', 'GET', '{{ route('listNews') }}', '{{ trans('messages.notification') }}', '{{ trans('messages.msg_delete_confirm') }}',
                  '{{ trans('messages.cancel') }}')" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.delete') }}"><i class="ti-trash"></i></button>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection