@extends('admin.layout.index')
@section('title')
{{ trans('messages.order_manament') }}
@endsection
@section('script')
<script src="{{ asset('resources/assets/admin/js/file-upload.js') }}"></script>
@parent
@endsection
@if(!empty(Session::get('toastData')))
@component('admin.commond.toast')
@slot('title')
{{ Session::get('toastData')['title'] }}
@endslot
@slot('message')
{{ Session::get('toastData')['message'] }}
@endslot
@slot('type')
{{ Session::get('toastData')['type'] }}
@endslot
@endcomponent
@endif
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Basic form elements</h4>
					<p class="card-description">
						Basic form elements
					</p>
					<form class="forms-sample" action="{{ route('settings') }}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="exampleInputName1">Name</label>
							<input type="text" class="form-control" name="name" id="exampleInputName1" placeholder="Name">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail3">Email address</label>
							<input type="email" name="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword4">Password</label>
							<input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
						</div>
						<div class="form-group">
							<label for="exampleSelectGender">Gender</label>
							<select class="form-control" id="exampleSelectGender">
								<option>Male</option>
								<option>Female</option>
							</select>
						</div>
						<div class="form-group">
							<label>File upload</label>
							<input type="file" name="file" class="file-upload-default" id="file">
							<div class="input-group col-xs-12">
								<input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
								<span class="input-group-append">
									<button class="file-upload-browse btn btn-primary" type="button">Upload</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label for="exampleInputCity1">City</label>
							<input type="text" class="form-control" id="exampleInputCity1" placeholder="Location">
						</div>
						<div class="form-group">
							<label for="exampleTextarea1">Textarea</label>
							<textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
						</div>
						<button type="submit" class="btn btn-primary mr-2">Submit</button>
						<button class="btn btn-light">Cancel</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection