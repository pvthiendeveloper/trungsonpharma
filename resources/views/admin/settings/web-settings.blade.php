@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_news') }}
@endsection
@section('script')
@parent
@endsection
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{ trans('messages.web_settings') }}</h4>
					<hr>
					<h5 class="card-description">{{ trans('messages.web_settings') }}</h5>
					<p class="card-description"><b>{{ trans('messages.note') }}:</b> {{ trans('messages.field_required') }}</p>
					@if(!empty($messageError))
					@component('admin.commond.alert')
					@slot('type')
					danger
					@endslot
					@slot('message')
					{{ $messageError }}
					@endslot
					@endcomponent
					@endif
					<form class="forms-sample" action="{{ route('webSettings') }}" method="POST">
						@csrf
						@if (!empty($settings))
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.introduce_video_url') }} *</label>
							<input type="text" class="form-control" name="introduceVideoUrl" value="{{ $settings->introduce_video_url }}" placeholder="{{ trans('messages.introduce_video_url') }}">
							@if($errors->has('introduceVideoUrl'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('introduceVideoUrl') }}
							</label>
							@endif
						</div>
						@endif
						<button type="submit" class="btn btn-primary mr-2">{{ trans('messages.submit') }}</button>
						<a class="btn btn-light" href="{{ route('listAccount') }}" on>{{ trans('messages.cancel') }}</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection