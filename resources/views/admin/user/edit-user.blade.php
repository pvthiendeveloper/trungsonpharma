@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_account') }}
@endsection
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">{{ trans('messages.manager_account') }}</h4>
					<hr>
					<h5 class="card-description">{{ trans('messages.edit_account') }}</h5>
					<p class="card-description"><b>{{ trans('messages.note') }}:</b> {{ trans('messages.field_required') }}</p>
					@if(!empty($messageError))
					@component('admin.commond.alert')
					@slot('type')
					danger
					@endslot
					@slot('message')
					{{ $messageError }}
					@endslot
					@endcomponent
					@endif
					<form class="forms-sample" action="{{ route('editAccount').'-'.$users->id }}" method="POST">
						@csrf
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.username') }} *</label>
							<input type="text" class="form-control" name="username" value="{{ $users->username }}" placeholder="{{ trans('messages.username') }}">
							@if($errors->has('username'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('username') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleSelectGender">{{ trans('messages.group_account') }} *</label>
							<select class="form-control" name="role">
								<option value="2" {{ ($users->role == '2' ? "selected":"") }}>{{ trans('messages.admin') }}</option>
								<option value="3" {{ ($users->role == '3' ? "selected":"") }}>{{ trans('messages.member') }}</option>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputName1">{{ trans('messages.fullname') }} *</label>
							<input type="text" class="form-control"" name="fullname" value="{{ $users->name }}" placeholder="{{ trans('messages.fullname') }}">
							@if($errors->has('fullname'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('fullname') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleInputEmail3">Email *</label>
							<input type="email" class="form-control" name="email" value="{{ $users->email }}" placeholder="Email">
							@if($errors->has('email'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('email') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleInputPassword4">{{ trans('messages.password') }}</label>
							<input type="password" class="form-control" name="password" placeholder="{{ trans('messages.password') }}">
							@if($errors->has('password'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('password') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleInputPassword4">{{ trans('messages.confirm_password') }}</label>
							<input type="password" class="form-control" name="password_confirmation"  placeholder="{{ trans('messages.confirm_password') }}">
							@if($errors->has('password_confirmation'))
							<label id="cname-error" class="error mt-2 text-danger" for="cname">
								<i class="fa fa-exclamation-circle"></i> {{ $errors->first('password_confirmation') }}
							</label>
							@endif
						</div>
						<div class="form-group">
							<label for="exampleSelectGender">{{ trans('messages.status') }}</label>
							<select class="form-control" id="exampleSelectGender" name="status">
								<option value="1" {{ ($users->status == '1' ? "selected":"") }}>{{ trans('messages.active') }}</option>
								<option value="0" {{ ($users->status == '0' ? "selected":"") }}>{{ trans('messages.block') }}</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary mr-2">{{ trans('messages.submit') }}</button>
						<a class="btn btn-light" href="{{ route('listAccount') }}" on>{{ trans('messages.cancel') }}</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection