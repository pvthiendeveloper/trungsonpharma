@extends('admin.layout.index')
@section('title')
{{ trans('messages.manager_account') }}
@endsection
@section('script')
@parent
<script src="{{ asset('resources/assets/admin/js/data-table.js') }}"></script>
@endsection
@if(!empty(Session::get('toastData')))
@component('admin.commond.toast')
@slot('title')
{{ Session::get('toastData')['title'] }}
@endslot
@slot('message')
{{ Session::get('toastData')['message'] }}
@endslot
@slot('type')
{{ Session::get('toastData')['type'] }}
@endslot
@endcomponent
@endif
@section('content')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">{{ trans('messages.list_account') }}</h4>
      <div class="row">
        <div class="col-12 table-responsive">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>ID #</th>
                <th>{{ trans('messages.fullname') }}</th>
                <th>{{ trans('messages.username') }}</th>
                <th>Email</th>
                <th>{{ trans('messages.role') }}</th>
                <th>{{ trans('messages.status') }}</th>
                <th>{{ trans('messages.actions') }}</th>
              </tr>
            </thead>
            <tbody>
              @if (empty($users))
              Empty
              @else
              @foreach ($users as $item)
              <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->username }}</td>
                <td>{{ $item->email }}</td>
                <td>
                  @switch($item->role)
                  @case(1)
                  Administrator
                  @break
                  @case(2)
                  {{ trans('messages.admin') }}
                  @break
                  @case(3)
                  {{ trans('messages.member') }}
                  @break
                  @default
                  {{ trans('messages.unknown') }}
                  @endswitch
                </td>
                <td>
                  @if ($item->status == 1)
                  <label class="badge badge-success">{{ trans('messages.active') }}</label>
                  @else
                  <label class="badge badge-danger">{{ trans('messages.block') }}</label>
                  @endif
                </td>
                <td>
                  <a href="{{ route('editAccount').'-'.$item->id}}">
                    <button type="button" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.edit') }}"><i class="ti-pencil"></i></button>
                  </a>
                  {{-- <a href="{{ route('deleteAccount').'-'.$item->id}}"> --}}
                    <button type="button" onclick="showSwalConfirm('{{ route('deleteAccount').'-'.$item->id}}', '', 'GET', '{{ route('listAccount') }}', '{{ trans('messages.notification') }}', '{{ trans('messages.msg_delete_confirm') }}',
                     '{{ trans('messages.cancel') }}')" class="btn btn-icons btn-inverse-light" style="padding: 0px;" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.delete') }}"><i class="ti-trash"></i></button>
                  {{-- </a> --}}
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection