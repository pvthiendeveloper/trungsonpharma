@extends('client.layout.index')
@section('urlVi')
{{ $url_vi }}
@endsection
@section('urlEn')
{{ $url_en }}
@endsection
@section('app_title')
{{ trans('messages.app_name') }}
@endsection
@section('content')
<div class="uk-container-large uk-margin-auto">

    <dir class="uk-section uk-text-center uk-margin-remove app-border-bottom app-header-content">
        <h1 class="uk-text-uppercase  app-color-text-primary">{{ trans('messages.contact') }}</h1>
    </dir>

    <div class="uk-section uk-padding-remove">
        <div class="uk-margin-remove uk-padding-large" uk-grid>
            <div class="uk-width-expand@m app-padding-res">
                <div class="app-form uk-padding-small">
                    <h3>
                        {{ trans('messages.contact_to_schedule') }}
                    </h3>
                    <form class="uk-form-stacked" action="{{ route('booking') }}" method="POST">
                     @csrf
                     <div class="uk-margin">
                        <div class="" uk-grid>
                            <div class="uk-width-1-2">
                                <label class="uk-form-label" for="form-stacked-fullname">{{ trans('messages.fullname') }} *</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="form-stacked-fullname" type="text" name="fullName" value="{{ old('fullName') }}">
                                </div>
                                @if ($errors->has('fullName'))
                                <label class="uk-form-label" for="form-stacked-fullname" style="color: #fd1212;">{{ $errors->first('fullName') }}</label>
                                @endif
                            </div>
                            <div class="uk-width-1-2 uk-padding-left-remove">
                                <label class="uk-form-label" for="form-stacked-phone">{{ trans('messages.phone_number') }} *</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="form-stacked-phone" type="text" name="phoneNumber" {{ old('phoneNumber') }}>
                                </div>
                                @if ($errors->has('phoneNumber'))
                                <label class="uk-form-label" for="form-stacked-phone" style="color: #fd1212;">{{ $errors->first('phoneNumber') }}</label>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-stacked-email">Email *</label>
                        <div class="uk-form-controls">
                            <input class="uk-input" id="form-stacked-email" type="text" name="email" {{ old('email') }}>
                        </div>
                        @if ($errors->has('email'))
                        <label class="uk-form-label" for="form-stacked-phone" style="color: #fd1212;">{{ $errors->first('email') }}</label>
                        @endif
                    </div>

                    <div class="uk-margin">
                        <div class="" uk-grid>
                            <div class="uk-width-1-2">
                                <label class="uk-form-label" for="form-stacked-zalo">Zalo</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="form-stacked-zalo" type="text" name="zalo">
                                </div>
                            </div>
                            <div class="uk-width-1-2 uk-padding-left-remove">
                                <label class="uk-form-label" for="form-stacked-viber">Facebook</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input" id="form-stacked-viber" type="text" name="facebook">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="uk-margin">
                        <div class="" uk-grid>
                            @if (count($bookServices) > 0)
                            <div class="uk-width-1-2 uk-flex uk-flex-column app-para">
                                @for ($index = 0; $index <= ($bookServices->count()) / 2; $index++)
                                <label>
                                    <input class="uk-checkbox" type="checkbox" name="service[{{ $index }}]" value="{{ $bookServices[$index]->id }}"> {{ $bookServices[$index]->name }}
                                </label>
                                @endfor
                            </div>
                            <div class="uk-width-1-2 uk-flex uk-flex-column app-para">
                                @for ($index = (int)(($bookServices->count()) / 2 + 1); $index < $bookServices->count(); $index++)
                                <label>
                                    <input class="uk-checkbox" type="checkbox" name="service[{{ $index + (int)(($bookServices->count()) / 2 + 1) }}]" value="{{ $bookServices[$index]->id }}"> {{ $bookServices[$index]->name }}
                                </label>
                                @endfor
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="uk-margin">
                        <p>{{ trans('messages.message') }}</p>
                        <textarea class="uk-textarea" rows="5" name="message"></textarea>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-width-expand">
                            <div class="uk-flex uk-flex-center">
                                <button type="submit" class="uk-button app-button">{{ trans('messages.send') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="uk-width-1-4@m uk-padding-remove">
            <div class="app-color-primary app-padding-medium">
                <h3 class="uk-text-uppercase app-color-text-white">
                    {{ trans('messages.contact') }}
                </h3>
                <div class="uk-margin-small">
                    <div class="uk-margin-remove" uk-grid>
                        <div class="uk-width-1-5 uk-padding-remove">
                            <span class="app-color-white" uk-icon="location"></span>
                        </div>
                        <div class="uk-width-4-5 uk-padding-remove app-color-text-white">
                            MG2-02 Vincom Shophouse, 209 Đường 30/4, P. Xuân Khánh, TP Cần
                            Thơ.
                        </div>
                    </div>
                </div>
                <div class="uk-margin-small">
                    <div class="uk-margin-remove" uk-grid>
                        <div class="uk-width-1-5 uk-padding-remove">
                            <span class="app-color-white" uk-icon="receiver"></span>
                        </div>
                        <div class="uk-width-4-5 uk-padding-remove app-color-text-white">
                            <a class="app-link" href="#">0931 789 199</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="uk-section uk-padding-remove">
    <iframe class="uk-width-expand" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15715.602301911453!2d105.774829!3d10.025063!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6e92dfada5b38ac0!2zVHJ1bmcgdMOibSBN4bu5IHBo4bqpbSBUcnVuZyBTxqFu!5e0!3m2!1svi!2s!4v1546159534910"
    width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

</div>
@endsection
