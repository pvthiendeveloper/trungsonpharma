<div class="uk-section uk-padding-large">
    <div class="" uk-grid>
        <div class="uk-width-1-2@m  uk-flex uk-flex-middle">
            <div class="uk-width-expand uk-position-relative uk-light" uk-slideshow="autoplay: true; autoplay-interval: 3000;">
                <ul class="uk-slideshow-items">
                    @if(!empty($imagesActivity))
                        @foreach($imagesActivity as $item)
                            <li>
                                <img src="{{ $item->image_url }}" alt="" uk-cover>
                            </li>
                        @endforeach
                    @endif
                </ul>
                <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
            </div>
        </div>
        <div class="uk-width-1-2@m uk-flex uk-flex-center uk-flex-middle app-color-text-primary">
            <h1 class="uk-text-center app-color-text-primary">
                Hoạt động, sự kiện các bác sĩ tại Thẩm Mỹ Viện
            </h1>
        </div>
    </div>
</div>
