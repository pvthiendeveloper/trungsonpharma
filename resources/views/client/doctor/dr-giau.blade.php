@extends('client.layout.index')
@section('urlVi')
{{ $url_vi }}
@endsection
@section('urlEn')
{{ $url_en }}
@endsection
@section('app_title')
{{ trans('messages.app_name') }}
@endsection
@section('content')
<div class="uk-container-large uk-margin-auto">
    @include('client.doctor.general-photos')

    <!-- doctor section -->
    <div class="uk-section uk-padding-remove">
       {{-- <div class="uk-background-cover uk-light uk-flex uk-margin-remove" uk-parallax="bgy: -400" style="background-image: url('{{ asset('resources/assets/client/images/background.jpg') }}');" --}}
        <div class="uk-background-cover uk-light uk-flex uk-margin-remove" uk-parallax="bgy: -400"  uk-grid>

        <div class="uk-width-1-2@m app-color-text-primary uk-padding-large">
            <h1 class="app-text-size-medium app-color-text-primary">
                ThS.BS. NGUYỄN HỮU GIÀU
            </h1>

            <p class="app-font-normal uk-text-justify app-color-text-primary">
                - Tốt nghiệp Bác sĩ đa khoa, Thạc sĩ Y học loại giỏi, chuyên ngành Ngoại khoa, Định hướng tạo
                hình thẩm mỹ.
                <br>
                - Chứng nhận tham dự “The AESTHETIC PLASTIC SURGERY COURSE, the 17th Annual International
                Congress on Aesthetic Plastic Surgery, the Safety in Aesthetic Medicine & Surgery”, TP.HCM.
                <br>
                - Chứng nhận đào tạo liên tục “Khóa đào tạo Phẫu thuật thẩm mỹ quốc tế IMAPS 2017 và Hội nghị
                Khoa học quốc tế thường niên lần thứ 16 của Hội phẫu thuật thẩm mỹ TP.HCM, chuyên đề Cập nhật
                kiến thức phẫu thuật thẩm mỹ và thẩm mỹ nội khoa”, Hội Y học TP.HCM.
                <br>
                - Chứng nhận tham dự “The 16th Annual International Scientific Congress on Aesthetic Plastic
                Surgery, the 10 year Anniversary of The Ho Chi Minh City Society of Plastic & Aesthetic
                Surgery, and The International meeting on Aesthetic plastic surgery IMAPS SAIGON 2017”, TP.
                HCM.
                <br>
                - Chứng nhận tham gia lớp tập huấn “Đào tạo kỹ thuật tiêm làm đầy bằng các dòng sản phẩm
                Regenyal Idea.
                <br>
                – Công nghệ Biorevolumetria”, “Ứng dụng laser trong y học và chăm sóc da thẩm
                mỹ”, “Cập nhật kiến thức y khoa với chuyên đề về sản phẩm Regenyal Idea.
                <br>
                – Công nghệ tái tạo và
                làm đẹp thế hệ mới”, “Cập nhật một số tiến bộ về điều trị & chăm sóc sẹo mụn trứng cá”.

            </p>
            <a href="{{ route('booking') }}">
                <button class="uk-button uk-button-small app-color-text-white app-button uk-margin-small-top uk-margin-small-bottom app-button-register">{{ trans('messages.booking') }}</button>
            </a>
        </div>
        <div class="uk-width-1-2@m uk-flex uk-flex-center uk-flex-top uk-padding-remove">
            <img class="uk-margin-auto" src="{{ asset('resources/assets/client/images/BS Giau.png') }}" alt="">
        </div>
    </div>
</div>
@include('client.doctor.activity')

</div>

@endsection
