@extends('client.layout.index')
@section('urlVi')
{{ $url_vi }}
@endsection
@section('urlEn')
{{ $url_en }}
@endsection
@section('app_title')
{{ trans('messages.app_name') }}
@endsection
@section('content')
<div class="uk-container-large uk-margin-auto">

	<!-- images section -->
	 @include('client.doctor.general-photos')

	<!-- doctor section -->
	<div class="uk-section uk-padding-remove">
		{{-- <div class="uk-background-cover uk-light uk-flex uk-margin-remove" uk-parallax="bgy: -400" style="background-image: url('{{ asset('resources/assets/client/images/background.jpg') }}');" --}}
		<div class="uk-background-cover uk-light uk-flex uk-margin-remove" uk-parallax="bgy: -400" 	uk-grid>

		<div class="uk-width-1-2@m app-color-text-black uk-padding-large">
			<h1 class="app-text-size-medium app-color-text-primary">
				ThS.BS. LÝ QUANG HUY
			</h1>

			<p class="app-font-normal uk-text-justify app-color-text-primary">
				- Tốt nghiệp Bác sĩ đa khoa, Thạc sĩ Y học loại giỏi, chuyên ngành Ngoại khoa, Định hướng tạo
				hình thẩm mỹ.
				<br>
				- Chứng nhận tham dự lớp tập huấn “Đào tạo kỹ thuật tiêm làm đầy bằng các dòng sản phẩm
				Regenyal Idea – Công nghệ Biorevolumetria”, TP HCM.
				<br>
				- Chứng nhận tham dự “The 1st ISAPS course VietNam 2016 & the 14th Annual International
				Scientific Congress of HSPAS” on The Achievements of Aesthetic & Plastic surgery”, TP.HCM.
				<br>
				- Chứng nhận tham dự “ ATEP 1st Outreach Program”, BV Chợ Rẫy, TP HCM.
			</p>
			<a href="{{ route('booking') }}">
                <button class="uk-button uk-button-small app-color-text-white app-button uk-margin-small-top uk-margin-small-bottom app-button-register">{{ trans('messages.booking') }}</button>
            </a>
		</div>
		<div class="uk-width-1-2@m uk-flex uk-flex-center uk-flex-top uk-padding-remove">
			 <img class="uk-margin-auto" src="{{ asset('resources/assets/client/images/BS Huy.png') }}" alt="">
		</div>
	</div>
</div>

@include('client.doctor.activity')
</div>
@endsection
