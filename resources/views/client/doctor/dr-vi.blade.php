@extends('client.layout.index')
@section('urlVi')
{{ $url_vi }}
@endsection
@section('urlEn')
{{ $url_en }}
@endsection
@section('app_title')
{{ trans('messages.app_name') }}
@endsection
@section('content')
<div class="uk-container-large uk-margin-auto">
    <!-- images section -->
     @include('client.doctor.general-photos')

    <!-- doctor section -->
    <div class="uk-section uk-padding-remove">
        <div class="uk-background-cover uk-light uk-flex uk-margin-remove" uk-parallax="bgy: -400");"
        uk-grid>

        <div class="uk-width-1-2@m app-color-text-black uk-padding-large">
            <h1 class="app-text-size-medium app-color-text-primary">
                BS. Trần Võ Thúy Vy
            </h1>

            <p class="app-font-normal uk-text-justify app-color-text-primary">
                - Tốt nghiệp Thủ khoa Bác sĩ chuyên ngành Y đa khoa.
                <br>
                - Giảng viên bộ môn da liễu Đại Học Y dược Cần Thơ.
                <br>
                - Chứng chỉ Laser trong y học và chăm sóc da thẩm mỹ.

            </p>
            <a href="{{ route('booking') }}">
                <button class="uk-button uk-button-small app-color-text-white app-button uk-margin-small-top uk-margin-small-bottom app-button-register">{{ trans('messages.booking') }}</button>
            </a>
        </div>
        <div class="uk-width-1-2@m uk-flex uk-flex-center uk-flex-bottom uk-padding-remove">
            <img class="uk-margin-auto" src="https://via.placeholder.com/570x800" alt="">
        </div>
    </div>
</div>

@include('client.doctor.activity')
</div>
@endsection
