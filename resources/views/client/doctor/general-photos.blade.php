<!-- images section -->
<div class="uk-section uk-padding-remove">
	<div class="image-header">
		<div class="img-container">
			<img class="img-data" data-src="{{ asset('resources/assets/client/images/banner-doctor.jpg') }}" src="{{ asset('resources/assets/client/images/banner-doctor.jpg') }}" alt="" uk-img>
		</div>
	</div>
</div>