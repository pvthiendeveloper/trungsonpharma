@extends('client.layout.index')
@if(!empty(Session::get('toastData')))
@component('client.commond.alert')
@slot('message')
{{ Session::get('toastData')['message'] }}
@endslot
@endcomponent
@endif
@section('app_title')
{{ trans('messages.app_name') }}
@endsection
@section('content')
<div class="uk-container-large uk-margin-auto">
	<!-- video section -->
	{{-- @if (!empty($settings)) --}}
        <div class="uk-section uk-padding-remove">
            <div class="image-header">
                <div class="img-container">
					<img class="img-data" data-src="{{ asset('resources/assets/client/images/banner_home.jpg') }}" alt="" uk-img>
				</div>
				<div class="app-intro-text">
                        <div class="wrapper">
                            <p class="text-flip title intro-1">THẨM MỸ VIỆN TRUNG SƠN</p>
                            <p class="text-flip intro-2">MG2-02 Vincom Shophouse, 209 Đường 30/4, P. Xuân Khánh, TP Cần Thơ</p>
                            <p class="text-flip intro-3">Hotline: 0931 789 199</p>
                        </div>
                    </div>
            </div>
        </div>
		{{-- @endif --}}
		<!-- text section -->
		<div class="uk-section uk-padding-small">
			<div class="uk-text-uppercase uk-text-center app-font-heading text-animation app-color-text-primary">
				<h1 class="uk-text-uppercase app-color-text-primary app-margin-top-small">Lý do chọn chúng tôi</h1>
			</div>
		</div>

		<!-- service section -->
		<div class="uk-section uk-padding-remove app-font-title">
			<div class="uk-child-width-1-5@m uk-grid-collapse uk-grid-match uk-padding-small" uk-grid>
				<!-- [begin] items -->
				<div class="item-slide">
					<div class="item-slide-container">
						<div class="item-slide-header">
							<h1 class="item-slide-title app-color-text-primary">01</h1>
						</div>

						<div class="item-slide-body app-color-text-primary">
							Đội ngũ Thạc sĩ - Bác sĩ chuyên môn cao trực tiếp thực hiện
						</div>
					</div>
				</div>

				<div class="item-slide">
					<div class="item-slide-container uk-margin-remove">
						<div class="item-slide-header">
							<h1 class="item-slide-title app-color-text-primary">02</h1>
						</div>
						<div class="item-slide-body app-color-text-primary">
							Công nghệ tiên tiến Renophase (Pháp), HIFU, Laser Fraxium, Laser Cellox (Châu Âu)
						</div>
					</div>
				</div>

				<div class="item-slide">
					<div class="item-slide-container uk-margin-remove">
						<div class="item-slide-header">
							<h1 class="item-slide-title app-color-text-primary">03</h1>
						</div>
						<div class="item-slide-body app-color-text-primary">
							Phòng phẫu thuật, phòng điều trị, trang thiết bị đạt chuẩn
						</div>
					</div>
				</div>

				<div class="item-slide">
					<div class="item-slide-container uk-margin-remove">
						<div class="item-slide-header">
							<h1 class="item-slide-title app-color-text-primary">04</h1>
						</div>
						<div class="item-slide-body app-color-text-primary">
							Tọa lạc tại vị trí sang trọng, đẳng cấp Vincome Shophouse
						</div>
					</div>
				</div>

				<div class="item-slide">
					<div class="item-slide-container uk-margin-remove">
						<div class="item-slide-header">
							<h1 class="item-slide-title app-color-text-primary">05</h1>
						</div>
						<div class="item-slide-body app-color-text-primary">
							Thẩm mỹ viện Trung Sơn thuộc hệ thống nhà thuốc Trung Sơn uy tín
						</div>
					</div>
				</div>
				<!-- [end] items -->
			</div>
			<!-- end service -->
		</div>

		<!-- customer comments section -->
		<div class="uk-section app-color-primary">
			<div class="uk-padding-remove uk-margin-remove" uk-grid>
				<div class="uk-width-2-5@m">
					<div class="uk-flex uk-flex-right@m uk-flex-center uk-padding-remove">
						<div class="uk-margin-medium-left uk-margin-medium-right">
							<h1 class="app-color-text-white app-font-heading uk-text-uppercase uk-padding-large uk-padding-remove-top uk-text-right uk-margin-remove">Cảm
								nhận<br>khách
							hàng</h1>
							<div class="uk-visible@m app-font-title app-color-text-white uk-text-uppercase uk-text-right uk-padding-large uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left">
								chia sẻ của khách hàng đã điều trị tại {{ trans('messages.app_name') }}
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-3-5@m uk-padding-large uk-padding-remove-top uk-padding-remove-bottom" uk-grid>
					<div class="uk-flex uk-flex-middle">
						<blockquote cite="#">
							<p class="uk-margin-small-bottom uk-text-justify app-font-normal app-color-text-white">“Xin
								cảm ơn Bác sĩ Huy và Bác sĩ Giàu đã giúp chị trẻ đẹp thêm 10 tuổi. Hai vết nhăn sâu
								trên mũi giữa đôi lông mày và hai vết nhăn bên khóe miệng của mình giờ đã biến mất
								nhờ chất làm đầy (filller) trong thẩm mỹ của {{ trans('messages.app_name') }}. Cám ơn hai em”
							</p>
							<footer class="uk-text-right app-color-text-white">Facebook Lily Truong</footer>
						</blockquote>
					</div>
				</div>
			</div>
		</div>

		<!-- slide images section -->
		@include('client.home.slider')

		<!-- text section -->
		<div class="uk-section uk-padding-remove">
			<div class="uk-grid-small uk-child-width-expand@s uk-margin-remove uk-flex uk-flex-middle" uk-grid>
				<div class="uk-width-1-2@m uk-padding-large">
					<div class="uk-flex uk-flex-center uk-padding-small">
						<img class="uk-height-1-1" height="190" width="190" data-src="{{ asset('resources/assets/client/images/logo-primary.png') }}" alt=""
						uk-img>
					</div>
					<div class="uk-flex uk-flex-center uk-padding-small">
						<h1 class="app-font-heading uk-text-center uk-text-uppercase uk-margin-remove app-color-text-primary">THẨM MỸ VIỆN<br>TRUNG SƠN</h1>
					</div>
				</div>
				<div class="uk-width-1-2@m uk-margin-remove uk-padding-remove">
					<div class="uk-flex uk-flex-middle">
						<div class="uk-text-justify uk-padding-large app-font-normal app-color-text-primary">
							<p>
								{{ trans('messages.app_name') }} là trung tâm chăm sóc sắc đẹp thuộc Hệ thống nhà thuốc Trung Sơn,
								được thành lập vào tháng 7/2017, tọa lạc tại vị trí sang trọng,
								đẳng cấp của Khu Vincom Shophouse, Xuân Khánh - Cần Thơ.
							</p>
							<p>
								Ra đời trong giai đoạn đỉnh cao của xu hướng thẩm mỹ và công nghệ làm đẹp,
								{{ trans('messages.app_name') }} hiểu rõ mong muốn và nhu cầu của chị em phụ nữ châu Á trong việc làm đẹp và thay đổi bản thân.
								{{ trans('messages.app_name') }} là nơi làm dịch vụ chuyên nghiệp,
								khoa học do đội ngũ bác sĩ chuyên môn cao, tốt nghiệp loại giỏi trực tiếp phụ trách, cam kết đem đến cho khách hàng vẻ đẹp toàn diện nhất.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- headline section -->
		<!-- images section -->
		<div class="uk-section uk-padding-remove">
			<div class="uk-padding-small">
				<div class="uk-child-width-1-3@m uk-child-width-1-2@s uk-grid-small uk-grid-match" uk-grid>
					@foreach ($newsHot as $item)
					<div>
                        <div class="uk-card uk-card-default">
                            <div class="uk-card-media-top">
                                <div class="img-ratio-16-9">
                                    <a class="img-container uk-width-expand uk-link-reset uk-inline-clip uk-transition-toggle"
                                        tabindex="0" href="{{ $currentUrl."/".trans('messages.lang')."/".trans('routes.news')."/".$item->getAlias() }}">
                                        <img class="img-fit uk-width-expand uk-transition-scale-up uk-transition-opaque"
                                            src="{{ $item->image }}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="uk-card-body uk-padding-small">
                                <h3 class="uk-card-title uk-text-uppercase app-color-text-primary app-card-left">
                                    <a class="uk-link-reset app-title-truncate app-font-normal" href="{{ $currentUrl."/".trans('messages.lang')."/".trans('routes.news')."/".$item->getAlias() }}"
                                        uk-tooltip="title: {{ $item->getTitle() }}; pos: top">{{ $item->getTitle() }}</a>
                                </h3>
                            </div>
                        </div>
                    </div>
					@endforeach
				</div>
			</div>
		</div>

	</div>
	@endsection
