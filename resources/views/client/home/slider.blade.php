<div class="uk-section uk-padding-remove gray">
	<div uk-slider="autoplay: true; autoplay-interval: 3000;">
		<div class="uk-slider-container uk-position-relative uk-light">
			<ul class="uk-slider-items uk-child-width-1-1">
				@if (!empty($banner))
				<li>
					<a href="{{ $banner->getLink1() }}"><img src="{{ $banner->image_1 }}" alt="">
						<div class="uk-position-top-center uk-position-large uk-panel">
							<h1 class="app-color-text-primary">{{ $banner->getTitle1() }}</h1>
						</div>
					</a>
				</li>
				<li>
					<a href="{{ $banner->getLink2() }}"><img src="{{ $banner->image_2 }}" alt="">
						<div class="uk-position-top-center uk-position-large uk-panel">
							<h1 class="app-color-text-primary">{{ $banner->getTitle2() }}</h1>
						</div>
					</a>
				</li>
				<li>
					<a href="{{ $banner->getLink3() }}"><img src="{{ $banner->image_3 }}" alt="">
						<div class="uk-position-top-center uk-position-large uk-panel">
							<h1 class="app-color-text-primary">{{ $banner->getTitle3() }}</h1>
						</div>
					</a>
				</li>
				<li>
					<a href="{{ $banner->getLink4() }}"><img src="{{ $banner->image_4 }}" alt="">
						<div class="uk-position-top-center uk-position-large uk-panel">
							<h1 class="app-color-text-primary">{{ $banner->getTitle4() }}</h1>
						</div>
					</a>
				</li>
				<li>
					<a href="{{ $banner->getLink5() }}"><img src="{{ $banner->image_5 }}" alt="">
						<div class="uk-position-top-center uk-position-large uk-panel">
							<h1 class="app-color-text-primary">{{ $banner->getTitle5() }}</h1>
						</div>
					</a>
				</li>
				<li>
					<a href="{{ $banner->getLink6() }}"><img src="{{ $banner->image_6 }}" alt="">
						<div class="uk-position-top-center uk-position-large uk-panel">
							<h1 class="app-color-text-primary">{{ $banner->getTitle6() }}</h1>
						</div>
					</a>
				</li>
				@endif
				{{-- <li>
					<img src="{{ asset('resources/assets/client/images/slider1.jpg') }}" alt="">
				</li>
				<li>
					<img src="{{ asset('resources/assets/client/images/slider2.jpg') }}" alt="">
				</li>
				<li>
					<img src="{{ asset('resources/assets/client/images/slider3.jpg') }}" alt="">
				</li>
				<li>
					<img src="{{ asset('resources/assets/client/images/slider4.jpg') }}" alt="">
				</li>
				<li>
					<img src="{{ asset('resources/assets/client/images/slider5.jpg') }}" alt="">
				</li>
				<li>
					<img src="{{ asset('resources/assets/client/images/slider6.jpg') }}" alt="">
				</li> --}}
			</ul>
			<a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			<a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		</div>
	</div>
</div>