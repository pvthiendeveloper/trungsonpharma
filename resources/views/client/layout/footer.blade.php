 <!-- footer section -->
 <div class="uk-section uk-container-large uk-margin-auto app-color-primary uk-padding-remove">
    <div class="uk-child-width-1-2@m uk-padding-large uk-margin-remove" uk-grid>
        <div class="app-color-text-white uk-padding-remove">
            <div class="uk-width-1-2">
                <img class="app-logo-bottom" src="{{ asset('resources/assets/client/images/logo.png') }}" alt="logo" uk-img>
            </div>
            <p class="app-para">
                <span uk-icon="location"></span> MG2-02 Vincom Shophouse, 209 Đường 30/4, P. Xuân Khánh, TP Cần
                Thơ.
                <br>
                <span uk-icon="receiver"></span> Hotline: 0931 789 199
            </p>
        </div>
        <div class="uk-padding-remove uk-margin-remove">
            <div class="fb-page" data-width="340" data-href="https://www.facebook.com/TrungSon.Cosmetic.Skincare"
            data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
            data-show-facepile="true">
            <blockquote cite="https://www.facebook.com/TrungSon.Cosmetic.Skincare" class="fb-xfbml-parse-ignore"><a
                href="https://www.facebook.com/TrungSon.Cosmetic.Skincare">{{ trans('messages.app_name') }}</a></blockquote>
        </div>
    </div>
</div>
</div>
<!-- end footer -->

<!-- coporight section -->
<div class="uk-section uk-padding-remove app-color-dark">
    <div class="uk-text-center uk-margin-small-top uk-margin-small-bottom app-color-text-white">© Trung Son
        Aesthetic Center. All Rights
        Reserved.
    </div>
</div>
<!-- end coporight -->

<!-- tel -->
<a class="app-call-btn" href="tel:+84931789199">
    <div class="call-layout">
        <div class="app-pulse">
        </div>
        <div class="call-circle">
            <img class="call-icon" src="{{ asset('resources/assets/client/images/call.svg') }}" alt="">
        </div>
    </div>
    <div class="call-number uk-flex uk-flex-center uk-flex-middle">
        <div>0931 789 199</div>
    </div>
</a>
    <!-- end tel -->