<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('app_title')</title>
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:400,500,600" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('resources/assets/admin/images/logo.png') }}">

    <link rel="stylesheet" href="{{ asset('resources/assets/client/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/client/css/uikit.min.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/client/css/style.css') }}">
    <script src="{{ asset('resources/assets/client/js/uikit.min.js') }}"></script>
    <script src="{{ asset('resources/assets/client/js/uikit-icons.min.js') }}"></script>
    <script src="{{ asset('resources/assets/client/js/style.js') }}"></script>
    @yield('script')
</head>

<body class="uk-container uk-container-expand uk-padding-remove">
    <!-- Facebook sdk -->
    @include('client.commond.facebook-fanpage')
    <!-- End facebook sdk -->

    <!-- navigation bar -->
    @include('client.layout.menu')
    <!-- end navbar -->

    <!-- content -->
    @yield('content')
    <!-- end content -->

    <!-- footer section -->
    @include('client.layout.footer')
    <!-- end footer -->
</body>

</html>
