<div class="app-navbar uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-box-shadow-small"
uk-sticky="offset: 0;">
<nav class="uk-navbar-container uk-container-large uk-margin-auto" uk-navbar>
    <div class="uk-navbar-left uk-flex uk-flex-center uk-flex-middle">
        <a class="uk-logo uk-flex uk-flex-center uk-flex-middle" href="{{ route('home') }}">
            <img src="{{ asset('resources/assets/client/images/logo.png') }}" alt="logo">
        </a>
    </div>

    <div class="uk-navbar-right app-hidden-nav">
        <a class="uk-navbar-toggle" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-usage" href="#"></a>
    </div>
    <div class="uk-navbar-right app-visible-nav">

        <ul class="uk-navbar-nav">
            @foreach ($categories as $item)
            <li>
                @if ($item->parent_id == -1)
                    @if ($item->getCategoryChilds()->count() > 0)
                            @if($item->id != 2)
                                <a href="{{ $currentUrl."/".trans('messages.lang')."/".$item->getAlias() }}">{{ $item->getName() }}</a>
                            @else
                                <a href="javascript:void(0)">{{ $item->getName() }}</a>
                            @endif
                    @else
                     <a href="{{ $currentUrl."/".trans('messages.lang')."/".$item->getAlias()}}">
                        {{ $item->getName() }}
                    </a>
                    @endif
                    <div class="uk-navbar-dropdown">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                           @foreach ($item->getCategoryChilds() as $child)
                           <li class="uk-nav-divider"></li>
                           <li><a class="uk-text-uppercase" href="{{ $currentUrl."/".trans('messages.lang')."/".$item->getAlias()."/".$child->getAlias() }}">{{ $child->getName() }}</a></li>
                           @endforeach
                       </ul>
                   </div>
                @else
               {{-- Do nothing --}}
                @endif
            </li>
            @endforeach
       <li class="uk-flex uk-flex-middle uk-margin-small-left uk-margin-small-right">
            <div>
                <a class="uk-margin-small-right" href="{{ $currentUrl .'/vi'}}@yield('urlVi')"><img src="{{ asset('resources/assets/client/images/flag-vietnam.png') }}" alt=""></a>
                <a class="uk-margin-small-right" href="{{ $currentUrl .'/en' }}@yield('urlEn')"><img src="{{ asset('resources/assets/client/images/flag-united-states.png') }}" alt=""></a>
            </div>
        </li>
        <li class="uk-flex uk-flex-middle">
            <div>
                <a class="uk-button app-button-register" href="{{ route('booking') }}">{{ trans('messages.booking') }}</a>
            </div>
        </li>
    </ul>

</div>
</nav>
</div>

<!-- off-canvas -->
<div id="offcanvas-usage" uk-offcanvas="flip: true; overlay: true;">
    <div class="uk-offcanvas-bar">

        <button class="uk-offcanvas-close" type="button" uk-close=""></button>

        <ul class="uk-nav-primary uk-nav-parent-icon" uk-nav="transition: ease">
            <li>
                <div>
                    <a class="uk-button app-button-register uk-margin-small-bottom" href="{{ route('booking') }}">{{ trans('messages.booking') }}</a>
                </div>
            </li>
             @foreach ($categories as $item)
                <li>
                    @if ($item->parent_id == -1)
                        @if ($item->getCategoryChilds()->count() > 0)
                            <li class="uk-parent">
                                <a href="javascript:void(0)">{{ $item->getName() }}</a>
                                <ul class="uk-nav-sub">
                                    @foreach ($item->getCategoryChilds() as $child)
                                       <li><a class="uk-text-uppercase" href="{{ $currentUrl."/".trans('messages.lang')."/".$item->getAlias()."/".$child->getAlias() }}">{{ $child->getName() }}</a></li>
                                    @endforeach
                                    @if($item->id != 2)
                                       <li><a class="uk-text-uppercase" href="{{ $currentUrl."/".trans('messages.lang')."/".$item->getAlias() }}">{{ trans('messages.all') }}</a></li>
                                    @endif
                                </ul>
                            </li>
                        @else
                            <a href="{{ $currentUrl."/".trans('messages.lang')."/".$item->getAlias()}}">
                                {{ $item->getName() }}
                            </a>
                            <ul class="uk-nav-sub">
                                @foreach ($item->getCategoryChilds() as $child)
                                   <li><a class="uk-text-uppercase" href="{{ $currentUrl."/".trans('messages.lang')."/".$item->getAlias()."/".$child->getAlias() }}">{{ $child->getName() }}</a></li>
                                @endforeach
                            </ul>
                        @endif

                    @else
                    {{-- Do nothing --}}
                    @endif
                </li>
           @endforeach
            <li>
                <a href="{{ $currentUrl .'/vi'}}@yield('urlVi')"><img src="{{ asset('resources/assets/client/images/flag-vietnam.png') }}" alt=""></a>
            </li>
            <li>
                <a href="{{ $currentUrl .'/en'}}@yield('urlEn')"><img src="{{ asset('resources/assets/client/images/flag-united-states.png') }}" alt=""></a>
            </li>
        </ul>
    </div>
</div>
    <!-- end off-canvas -->