@extends('client.layout.index')
@section('urlVi')
{{ $url_vi }}
@endsection
@section('urlEn')
{{ $url_en }}
@endsection
@section('app_title')
{{ $title }}
@endsection
@section('content')
<div class="uk-container-large uk-margin-auto">


	<div class="uk-section uk-text-center uk-margin-remove app-border-bottom app-header-content">
		<h1 class=" uk-text-uppercase app-color-text-primary">{{ $title }}</h1>
	</div>

	<div class="uk-section uk-padding-remove">
		<div class="uk-margin-remove app-padding" uk-grid>
			<div class="uk-width-3-4@m uk-padding-remove">
				<div class="app-list uk-padding-small">
					<div class="uk-child-width-1-3@m uk-child-width-1-2@s uk-grid-small uk-grid-match" uk-grid>
						@foreach ($newsChild as $item)
						<div>
							<div class="uk-card uk-card-default">
								<div class="uk-card-media-top">
									<div class="img-ratio-16-9">
										<a class="img-container uk-width-expand uk-link-reset uk-inline-clip uk-transition-toggle"
										tabindex="0" href="{{ $currentUrl."/".$segmentOneAndTrue."/".$item->getAlias() }}">
										<img class="img-fit uk-width-expand uk-transition-scale-up uk-transition-opaque"
										src="{{ $item->image }}" alt="">
									</a>
								</div>
							</div>
							<div class="uk-card-body uk-padding-small">
								<h3 class="uk-card-title uk-text-uppercase app-color-text-primary app-card-left">
									<a class="uk-link-reset app-title-truncate" href="{{ $currentUrl."/".$segmentOneAndTrue."/".$item->getAlias() }}" uk-tooltip="title: {{ $item->getTitle() }}; pos: top">{{ $item->getTitle() }}</a>
								</h3>
								<div class="uk-text-justify app-truncate app-card-right">
									<p>{!! $item->getShortDescription() !!}</p>
								</div>
							</div>
							<div class="uk-card-footer uk-padding-small">
								<a href="{{ $currentUrl."/".$segmentOneAndTrue."/".$item->getAlias() }}" class="uk-button uk-button-text"><span class="app-icon-margin-right"
									uk-icon="icon: link"></span>{{ trans('messages.view_more') }}</a>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
				<div class="app-pagination uk-flex uk-flex-center uk-padding-small">
					<ul class="uk-pagination uk-margin-remove">
						{{ $newsChild->links() }}
					</ul>
				</div>
			</div>
			@include('client.news.other-services')
		</div>
	</div>
</div>
@endsection