<div class="uk-width-1-4@m uk-padding-small uk-flex uk-flex-center">
	<div class="app-navbar-right uk-width-expand">
		<form class="uk-search uk-search-default uk-width-expand white">
			<span uk-search-icon></span>
			<input class="uk-search-input" type="search" placeholder="{{ trans('messages.search') }}">
		</form>
		<div class="uk-margin-medium-top">
			{{ trans('messages.otherServices') }}
		</div>
		@foreach ($services as $item)
		<div class="uk-padding-small uk-padding-remove-horizontal uk-padding-remove-bottom">
			<a href="{{ route('services', $item->getAlias()) }}" class="uk-link-reset">
				<div class="uk-width-expand uk-margin-remove app-hover-change-bg" uk-grid>
					<div class="uk-width-expand app-banner-item-content">
						<p class="uk-margin-remove uk-text-uppercase app-banner-item-title app-truncate-one-line">{{ $item->getTitle() }}</p>
						<p class="uk-margin-remove app-banner-item-desciption app-truncate-one-line">
						</div>
						<div class="app-item-banner">
							<div class="app-item-banner-img" data-src="{{ $item->image }}" uk-img>
							</div>
						</div>
					</div>
				</a>
			</div>
			@endforeach
			<div class="uk-flex uk-flex-center uk-margin-medium-top">
				<a href="{{ route('booking') }}" class="uk-link-reset">
					<img src="{{ asset('resources/assets/client/images/banner.jpg') }}" alt="">
				</a>
			</div>
		</div>
	</div>