<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@getHomePage')->middleware('client');
// Route::get('/', function () {
// 	return redirect(route('home'));
// })->middleware('client');

Route::get('/testdb', function () {
	try {
		DB::connection()->getPdo();
		return "Database connected.";
	} catch (\Exception $e) {
		return "Could not connect to the database.  Please check your configuration. error: " . $e;
	}
});

Route::get('vi', 'LanguageController@getSetLocalizeVi')->name('langVi');
Route::get('en', 'LanguageController@getSetLocalizeEn')->name('langEn');

// Localized
Route::localized(function () {
	Route::get(Lang::uri('login'), 'UsersController@getLoginAdmin')->name('login');
	Route::post(Lang::uri('login'), 'UsersController@postLoginAdmin')->name('login');

	Route::group(['prefix' => Lang::uri('admin'), 'middleware' => 'loginAdmin'], function () {
		Route::get(Lang::uri('dashboard'), 'HomeAdminController@getHomeAdminPage')->name('dashboard');
		Route::get(Lang::uri('logout'), 'HomeAdminController@getLogout')->name('logout');

		Route::group(['prefix' => Lang::uri('account')], function () {
			/*Account*/
			Route::get(Lang::uri('listAccount'), 'UsersController@getListUser')->name('listAccount');
			Route::get(Lang::uri('addAccount'), 'UsersController@getCreateUser')->name('addAccount');
			Route::post(Lang::uri('addAccount'), 'UsersController@postCreateUser')->name('addAccount');
			Route::get(Lang::uri('editAccount') . '-{id}', 'UsersController@getEditUser')->name('editAccount');
			Route::get(Lang::uri('editAccount'), 'UsersController@getEditUser')->name('editAccount');
			Route::post(Lang::uri('editAccount') . '-{id}', 'UsersController@postEditUser')->name('editAccount');
			Route::post(Lang::uri('editAccount'), 'UsersController@postEditUser')->name('editAccount');
			Route::get(Lang::uri('deleteAccount') . '-{id}', 'UsersController@getDeleteUser')->name('deleteAccount');
			Route::get(Lang::uri('deleteAccount'), 'UsersController@getDeleteUser')->name('deleteAccount');
		});

		Route::group(['prefix' => Lang::uri('category')], function () {
			/*Category*/
			Route::get(Lang::uri('listCategory'), 'CategoryController@getListCate')->name('listCategory');
			Route::get(Lang::uri('addCategory'), 'CategoryController@getCreateCate')->name('addCategory');
			Route::post(Lang::uri('addCategory'), 'CategoryController@postCreateCate')->name('addCategory');
			Route::get(Lang::uri('editCategory') . '-{id}', 'CategoryController@getEditCate')->name('editCategory');
			Route::get(Lang::uri('editCategory'), 'CategoryController@getEditCate')->name('editCategory');
			Route::post(Lang::uri('editCategory') . '-{id}', 'CategoryController@postEditCate')->name('editCategory');
			Route::post(Lang::uri('editCategory'), 'CategoryController@postEditCate')->name('editCategory');
			Route::get(Lang::uri('deleteCategory') . '-{id}', 'CategoryController@getDeleteCate')->name('deleteCategory');
			Route::get(Lang::uri('deleteCategory'), 'CategoryController@getDeleteCate')->name('deleteCategory');
		});

		Route::group(['prefix' => Lang::uri('news')], function () {
			/*News*/
			Route::get(Lang::uri('listNews'), 'NewsController@getListNews')->name('listNews');
			Route::get(Lang::uri('addNews'), 'NewsController@getCreateNews')->name('addNews');
			Route::post(Lang::uri('addNews'), 'NewsController@postCreateNews')->name('addNews');
			Route::get(Lang::uri('editNews') . '-{id}', 'NewsController@getEditNews')->name('editNews');
			Route::get(Lang::uri('editNews'), 'NewsController@getEditNews')->name('editNews');
			Route::post(Lang::uri('editNews') . '-{id}', 'NewsController@postEditNews')->name('editNews');
			Route::post(Lang::uri('editNews'), 'NewsController@postEditNews')->name('editNews');
			Route::get(Lang::uri('deleteNews') . '-{id}', 'NewsController@getDeleteNews')->name('deleteNews');
			Route::get(Lang::uri('deleteNews'), 'NewsController@getDeleteNews')->name('deleteNews');
		});

		Route::group(['prefix' => Lang::uri('booking')], function () {
			/*News*/
			Route::get(Lang::uri('listBooking'), 'BookingController@getListBooking')->name('listBooking');
			Route::get(Lang::uri('deleteBooking') . '-{id}', 'BookingController@getDeleteBooking')->name('deleteBooking');
			Route::get(Lang::uri('deleteBooking'), 'BookingController@getDeleteBooking')->name('deleteBooking');
			Route::get(Lang::uri('editBooking') . '-{id}', 'BookingController@getEditBooking')->name('editBooking');
			Route::get(Lang::uri('editBooking'), 'BookingController@getEditBooking')->name('editBooking');
			Route::post(Lang::uri('editBooking') . '-{id}', 'BookingController@postEditBooking')->name('editBooking');
			Route::post(Lang::uri('editBooking'), 'BookingController@postEditBooking')->name('editBooking');
		});

		Route::group(['prefix' => Lang::uri('images')], function () {
			/*Images*/
			Route::get(Lang::uri('listImages'), 'ImagesController@getListImages')->name('listImages');
			Route::get(Lang::uri('addImages'), 'ImagesController@getCreateImages')->name('addImages');
			Route::post(Lang::uri('addImages'), 'ImagesController@postCreateImages')->name('addImages');
			Route::get(Lang::uri('editImages') . '-{id}', 'ImagesController@getEditImages')->name('editImages');
			Route::get(Lang::uri('editImages'), 'ImagesController@getEditImages')->name('editImages');
			Route::post(Lang::uri('editImages') . '-{id}', 'ImagesController@postEditImages')->name('editImages');
			Route::post(Lang::uri('editImages'), 'ImagesController@postEditImages')->name('editImages');
			Route::get(Lang::uri('deleteImages') . '-{id}', 'ImagesController@getDeleteImages')->name('deleteImages');
			Route::get(Lang::uri('deleteImages'), 'ImagesController@getDeleteImages')->name('deleteImages');
		});

		Route::get(Lang::uri('bannerManagement'), 'BannerController@getEditBanner')->name('bannerManagement');
		Route::post(Lang::uri('bannerManagement'), 'BannerController@postEditBanner')->name('bannerManagement');

		Route::get(Lang::uri('settings'), 'SettingsController@getSettings')->name('settings');
		Route::post(Lang::uri('settings'), 'SettingsController@postSettings')->name('settings');

		Route::get(Lang::uri('webSettings'), 'SettingsController@getWebSettings')->name('webSettings');
		Route::post(Lang::uri('webSettings'), 'SettingsController@postWebSettings')->name('webSettings');

	});

	//Routes client
	Route::group(['middleware' => 'client'], function () {
		Route::get(Lang::uri('/'), 'HomeController@getHomePage')->name('/');
		Route::get(Lang::uri('home'), 'HomeController@getHomePage')->name('home');

		/*Booking*/
		Route::get(Lang::uri('booking'), 'HomeController@getBookingPage')->name('booking');
		Route::post(Lang::uri('booking'), 'HomeController@postCreateBooking')->name('booking');

		/*Doctor*/
		Route::get(Lang::uri('doctor') . '/' . Lang::uri('drHuy'), 'HomeController@getDoctorHuyPage')->name('drHuy');
		Route::get(Lang::uri('doctor') . '/' . Lang::uri('drGiau'), 'HomeController@getDoctorGiauPage')->name('drGiau');
		Route::get(Lang::uri('doctor') . '/' . Lang::uri('drVi'), 'HomeController@getDoctorViPage')->name('drVi');

		/*Services*/
		Route::get(Lang::uri('services'), 'NewsClientController@getServices')->name('services');
		Route::get(Lang::uri('services') . "/{alias}", 'NewsClientController@getServicesDetail')->name('services');

		/*News*/
		Route::get(Lang::uri('news'), 'NewsClientController@getNews')->name('news');
		Route::get(Lang::uri('news') . "/{alias}", 'NewsClientController@getNewsDetail')->name('news');

		/*Technology*/
		Route::get(Lang::uri('technology'), 'NewsClientController@getTechnology')->name('technology');
		Route::get(Lang::uri('technology') . "/{alias}", 'NewsClientController@getTechnologyDetail')->name('technology');

		/*Technology*/
		Route::get(Lang::uri('knowledgeOfBeauty'), 'NewsClientController@getKnowledgeOfBeauty')->name('knowledgeOfBeauty');
		Route::get(Lang::uri('knowledgeOfBeauty') . "/{alias}", 'NewsClientController@getKnowledgeOfBeautyDetail')->name('knowledgeOfBeauty');

	});

});
